# Movie Rama: Android Application

### Description
The **"Movie Rama"** app is a small movie repository where users can check the current popular movies list, search for movies of their interest by title and view a movie's detailed information.

### Version
The current code version of the Movie Rama app is **"Beta 3.0"**.

### Requirements
Movie Rama app operates with **Android 22 and above** and it is written in Java and XML.

### Author
NFG | Nikolaos Fragkiadakis - www.nikolaosfragkiadakis.com.

### Copyright
2020 © Nikolaos Fragkiadakis.

### License
The **"Movie Rama"** app is a non profit open source personal pet project and it operates under the MIT License.

### Contact
For any issues that may occur or for more information requests or even for your comments and feedback please contact the author directly using the following email address: info@nikolaosfragkiadakis.com

# Credits
A huge **"THANK YOU"** is owed to Mr. Georgios Tsaousidis and Mr. Dimitris-Sotiris Tsolis for their technical advices, their mental support, and their valuable thoughts and feedback.

The data and image source of the **"Movie Rama"** app is the result of the awesome work of the **"The Movie Database"** API community (https://www.themoviedb.org). This product uses the **"The Movie Database"** API but is not endorsed or certified by **"The Movie Database"**.
