<?xml version="1.0" encoding="utf-8"?><!-- MIT License

Copyright 2020 © Nikolaos Fragkiadakis / www.nikolaosfragkiadakis.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. -->
<ScrollView xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical">

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="vertical"
        android:padding="16dp">

        <ImageView
            android:layout_width="match_parent"
            android:layout_height="200dp"
            android:background="@drawable/bg_header_light"
            android:padding="16dp"
            android:src="@drawable/ic_app_logo"
            tools:ignore="ContentDescription" />

        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="16dp"
            android:layout_marginBottom="8dp"
            android:fontFamily="@font/dosis_bold"
            android:text="@string/app_name"
            android:textColor="@color/colorPrimaryDark"
            android:textSize="36sp" />

        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginBottom="24dp"
            android:letterSpacing="0.2"
            android:text="@string/more_information_screen_subtitle"
            android:textAllCaps="true" />

        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="16dp"
            android:layout_marginBottom="8dp"
            android:fontFamily="@font/dosis_bold"
            android:text="@string/app_description_header"
            android:textAllCaps="true" />

        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="@string/app_description" />

        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="16dp"
            android:layout_marginBottom="8dp"
            android:fontFamily="@font/dosis_bold"
            android:text="@string/app_version_header"
            android:textAllCaps="true" />

        <TextView
            android:id="@+id/app_version"
            android:layout_width="match_parent"
            android:layout_height="wrap_content" />

        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="16dp"
            android:layout_marginBottom="8dp"
            android:fontFamily="@font/dosis_bold"
            android:text="@string/developer_header"
            android:textAllCaps="true" />

        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="@string/developer" />

        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="16dp"
            android:layout_marginBottom="8dp"
            android:fontFamily="@font/dosis_bold"
            android:text="@string/copyright_header"
            android:textAllCaps="true" />

        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="@string/copyright" />

        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="16dp"
            android:layout_marginBottom="8dp"
            android:fontFamily="@font/dosis_bold"
            android:text="@string/license_header"
            android:textAllCaps="true" />

        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="@string/license" />

        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="16dp"
            android:layout_marginBottom="8dp"
            android:fontFamily="@font/dosis_bold"
            android:text="@string/issue_report_header"
            android:textAllCaps="true" />

        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="@string/issue_report_text" />

        <TextView
            android:id="@+id/issue_report_button"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="16dp"
            android:background="@drawable/bg_header_outline"
            android:clickable="true"
            android:focusable="true"
            android:gravity="center_vertical"
            android:letterSpacing="0.2"
            android:text="@string/issue_report_button"
            android:textAlignment="center"
            android:textAllCaps="true"
            android:textColor="@color/colorPrimaryDark" />

        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="16dp"
            android:layout_marginBottom="8dp"
            android:fontFamily="@font/dosis_bold"
            android:text="@string/credits_header"
            android:textAllCaps="true" />

        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="@string/credits" />

        <ImageView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:padding="24dp"
            android:src="@drawable/ic_tmdb_logo"
            tools:ignore="ContentDescription" />

    </LinearLayout>

</ScrollView>
