package com.nikolaosfragkiadakis.www.movierama.ui.dispatcher;

/* MIT License

Copyright 2020 © Nikolaos Fragkiadakis / www.nikolaosfragkiadakis.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.nikolaosfragkiadakis.www.movierama.ui.activities.PopularMovies;
import com.nikolaosfragkiadakis.www.movierama.utilities.Constants.DispatcherConstants;

/**
 * The {@link ActivityDispatcher} class is responsible to trigger the app at every lunch in
 * its last saved state.
 */
public class ActivityDispatcher extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Class<?> activityClass;

        try {
            SharedPreferences prefs = getSharedPreferences(DispatcherConstants.PREF_ACTIVITY_NAME_FIELD_TAG, MODE_PRIVATE);
            activityClass = Class.forName(prefs.getString(DispatcherConstants.PREF_LAST_ACTIVITY_FIELD_TAG, getClass().getName()));
        } catch (ClassNotFoundException e) {
            activityClass = PopularMovies.class;
        }

        startActivity(new Intent(this, activityClass));
    }
}
