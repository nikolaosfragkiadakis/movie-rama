package com.nikolaosfragkiadakis.www.movierama.datamodel.datapersistence;

/* MIT License

Copyright 2020 © Nikolaos Fragkiadakis / www.nikolaosfragkiadakis.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.nikolaosfragkiadakis.www.movierama.datamodel.datapersistence.MovieContract.MovieEntry;
import com.nikolaosfragkiadakis.www.movierama.utilities.Constants.MovieEntryConstants;
import com.nikolaosfragkiadakis.www.movierama.utilities.Constants.MovieDbHelperConstants;

/**
 * The {@link MovieDbHelper} class is the container of all the necessary helper
 * methods, which will provide the communication with the "movies" database.
 */
public class MovieDbHelper extends SQLiteOpenHelper {
    // The constructor class for the movie database helper class.
    MovieDbHelper(Context context) {
        super(context, MovieDbHelperConstants.DATABASE_NAME, null, MovieDbHelperConstants.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // Database schema:
        // CREATE TABLE favorites (_id INTEGER PRIMARY KEY, name TEXT NOT NULL);
        final String SQL_CREATE_ENTRIES = "CREATE TABLE " + MovieEntryConstants.FAVORITES_TABLE_NAME + " (" +
                MovieEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + MovieEntryConstants.COLUMN_MOVIE_ID + " TEXT NOT NULL);";

        sqLiteDatabase.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + MovieEntryConstants.FAVORITES_TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
