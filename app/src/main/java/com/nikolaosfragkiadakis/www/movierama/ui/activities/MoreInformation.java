package com.nikolaosfragkiadakis.www.movierama.ui.activities;

/* MIT License

Copyright 2020 © Nikolaos Fragkiadakis / www.nikolaosfragkiadakis.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.nikolaosfragkiadakis.www.movierama.BuildConfig;
import com.nikolaosfragkiadakis.www.movierama.R;
import com.nikolaosfragkiadakis.www.movierama.utilities.Constants.MoreInformationConstants;

public class MoreInformation extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.more_information_screen);

        // Set the visible title of the Popular Movies activity programmatically.
        this.setTitle(R.string.more_information_activity_name);

        // Retrieve the current app version name and update the UI accordingly.
        TextView mAppVersionTextView = findViewById(R.id.app_version);
        try {
            mAppVersionTextView.setText(BuildConfig.VERSION_NAME);
        } catch (Exception e) {
            mAppVersionTextView.setText(R.string.error_during_retrieving_the_app_version_name);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        // User clicked on the back button.
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Save the current state of the PopularMovieActivity.
        SharedPreferences prefs = getSharedPreferences(MoreInformationConstants.PREF_ACTIVITY_NAME_FIELD_TAG, MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(MoreInformationConstants.PREF_LAST_ACTIVITY_FIELD_TAG, getClass().getName());
        editor.apply();
    }
}
