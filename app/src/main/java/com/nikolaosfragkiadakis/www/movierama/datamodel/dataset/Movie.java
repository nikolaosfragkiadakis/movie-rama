package com.nikolaosfragkiadakis.www.movierama.datamodel.dataset;

/* MIT License

Copyright 2020 © Nikolaos Fragkiadakis / www.nikolaosfragkiadakis.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

import java.util.ArrayList;

/**
 * This is the custom Movie class.
 * A {@link Movie} instance represents a movie that will be shown to the user.
 * It contains all the necessary details of the movie.
 */
public class Movie {
    // 1. The ID of the movie.
    private int mID;
    // 2. The poster path of the movie.
    private String mPosterPath;
    // 3. The title of the movie.
    private String mTitle;
    // 4. The genres of the movie.
    private String mGenres;
    // 5. The release date of the movie.
    private String mReleaseDate;
    // 6. The rating number of the movie.
    private Long mRatingNumber;
    // 7. The description of the movie.
    private String mDescription;
    // 8. The director of the movie.
    private String mDirector;
    // 9. The cast of the movie.
    private String mCast;
    // 10. The review list of the movie.
    private ArrayList<Review> mReviewList;
    // 11. The similar movies list of the movie.
    private ArrayList<Movie> mSimilarMoviesList;

    /**
     * The default constructor of the custom Movie class.
     *
     * @param id          represents the id of the movie.
     * @param posterPath  represents the poster path of the movie.
     * @param title       represents the title of the movie.
     * @param genres      represents the genres of the movie
     * @param releaseDate represents the release date of the movie.
     * @param rating      represents the rating number of the movie.
     * @param description represents the description of the movie.
     * @param director    represents the director of the movie.
     * @param cast        represents the cast of the movie.
     * @param reviewList  represents the review list of the movie.
     * @param similarMoviesList represents the similar movies list of the movie.
     */
    public Movie(int id, String posterPath, String title, String genres,
                 String releaseDate, Long rating, String description,
                 String director, String cast, ArrayList<Review> reviewList,
                 ArrayList<Movie> similarMoviesList) {
        mID = id;
        mPosterPath = posterPath;
        mTitle = title;
        mGenres = genres;
        mReleaseDate = releaseDate;
        mRatingNumber = (rating > 0) ? rating / 2 : 0;
        mDescription = description;
        mDirector = director;
        mCast = cast;
        mReviewList = reviewList;
        mSimilarMoviesList = similarMoviesList;
    }

    // 1. Get the movie's id.
    public int getID() {
        return mID;
    }

    // 2. Get the movie's poster path.
    public String getPosterPath() {
        return mPosterPath;
    }

    // 3. Get the movie's title.
    public String getTitle() {
        return mTitle;
    }

    // 4. Get the movie's genres.
    public String getGenres() {
        return mGenres;
    }

    // 5. Get the movie's release date.
    public String getReleaseDate() {
        return mReleaseDate;
    }

    // 6. Get the movie's rating number.
    public Long getRatingNumber() {
        return mRatingNumber;
    }

    // 7. Get the movie's description.
    public String getDescription() {
        return mDescription;
    }

    // 8. Get the movie's director.
    public String getDirector() {
        return mDirector;
    }

    // 9. Get the movie's cast.
    public String getCast() {
        return mCast;
    }

    // 10. Get the movie's review list.
    public ArrayList<Review> getReviewList() {
        return mReviewList;
    }

    // 11. Get the movie's similar movies list.
    public ArrayList<Movie> getSimilarMoviesList() {return mSimilarMoviesList;}

    /**
     * The {@link Builder} method implements the builder constructor pattern for the Movie class.
     */
    public static class Builder {
        // 1. The ID of the movie.
        private int id;
        // 2. The poster path of the movie.
        private String posterPath;
        // 3. The title of the movie.
        private String title;
        // 4. The genres of the movie.
        private String genres;
        // 5. The release date of the movie.
        private String releaseDate;
        // 6. The rating number of the movie.
        private Long ratingNumber;
        // 7. The description of the movie.
        private String description;
        // 8. The director of the movie.
        private String director;
        // 9. The cast of the movie.
        private String cast;
        // 10. The review list of the movie.
        private ArrayList<Review> reviewList;
        // 11. The similar movies list of the movie.
        private ArrayList<Movie> similarMoviesList;

        // 1. Set the movie's id.
        public Builder setId(final int id) {
            this.id = id;
            return this;
        }

        // 2. Set the movie's poster path.
        public Builder setPosterPath(final String posterPath) {
            this.posterPath = posterPath;
            return this;
        }

        // 3. Set the movie's title.
        public Builder setTitle(final String title) {
            this.title = title;
            return this;
        }

        // 4. Set the movie's genres.
        public Builder setGenres(final String genres) {
            this.genres = genres;
            return this;
        }

        // 5. Set the movie's release date.
        public Builder setReleaseDate(final String releaseDate) {
            this.releaseDate = releaseDate;
            return this;
        }

        // 6. Set the movie's rating number.
        public Builder setRatingNumber(final Long ratingNumber) {
            this.ratingNumber = ratingNumber;
            return this;
        }

        // 7. Set the movie's description.
        public Builder setDescription(final String description) {
            this.description = description;
            return this;
        }

        // 8. Set the movie's director.
        public Builder setDirector(final String director) {
            this.director = director;
            return this;
        }

        // 9. Set the movie's cast.
        public Builder setCast(final String cast) {
            this.cast = cast;
            return this;
        }

        // 10. Set the movie's review list.
        public Builder setReviewList(final ArrayList<Review> reviewList) {
            this.reviewList = reviewList;
            return this;
        }

        // 11. Set the movie's similar movies list.
        public Builder setSimilarMoviesList(final ArrayList<Movie> similarMoviesList) {
            this.similarMoviesList = similarMoviesList;
            return this;
        }

        // Build the new movie's instance.
        public Movie build() {
            // Build and return a new "Movie" instance.
            return new Movie(this.id,
                             this.posterPath,
                             this.title,
                             this.genres,
                             this.releaseDate,
                             this.ratingNumber,
                             this.description,
                             this.director,
                             this.cast,
                             this.reviewList,
                             this.similarMoviesList);
        }
    }
}
