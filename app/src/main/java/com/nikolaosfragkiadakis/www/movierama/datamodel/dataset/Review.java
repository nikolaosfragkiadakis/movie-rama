package com.nikolaosfragkiadakis.www.movierama.datamodel.dataset;

/* MIT License

Copyright 2020 © Nikolaos Fragkiadakis / www.nikolaosfragkiadakis.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

/**
 * This is the custom Review class.
 * A {@link Review} instance represents a movie's review that will be shown to the user
 * at the movie details screen.
 * It contains all the necessary details of the review.
 */
public class Review {
    // 1. The author of the review.
    private String mReviewAuthor;
    // 2. The content of the review.
    private String mReviewContent;

    /**
     * The constructor of a new Review object. It will be used for fetching the
     * data set of the movie details screen.
     *
     * @param author      represents the author of the review.
     * @param content     represents the content of the review.
     */
    private Review(String author, String content) {
        mReviewAuthor = author;
        mReviewContent = content;
    }

    // 1. Get the review's author.
    public String getReviewAuthor() {
        return mReviewAuthor;
    }

    // 2. Get the review's content.
    public String getReviewContent() {
        return mReviewContent;
    }

    /**
     * The {@link Builder} method implements the builder constructor pattern for the Review class.
     */
    public static class Builder {
        // 1. The author of the review.
        private String reviewAuthor;
        // 2. The content of the review.
        private String reviewContent;

        // 1. Set the review's author.
        public Builder setReviewAuthor(final String reviewAuthor) {
            this.reviewAuthor = reviewAuthor;
            return this;
        }

        // 2. Set the review's content.
        public Builder setReviewContent(final String reviewContent) {
            this.reviewContent = reviewContent;
            return this;
        }

        // Build the new review's instance.
        public Review build() {
            // Build and return a new "Review" instance.
            return new Review(this.reviewAuthor,
                              this.reviewContent);
        }
    }
}
