package com.nikolaosfragkiadakis.www.movierama.dataloaders;

/* MIT License

Copyright 2020 © Nikolaos Fragkiadakis / www.nikolaosfragkiadakis.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

import android.content.Context;
import android.content.AsyncTaskLoader;

import com.nikolaosfragkiadakis.www.movierama.datamodel.dataset.Movie;
import com.nikolaosfragkiadakis.www.movierama.datarequest.ExtractMoviesData;

/**
 * Download the movie's data asynchronously so the UI does not get blocked and can interact
 * with the user's actions. Also, by using a "loader" the data that have been downloaded will
 * stay at the memory till the activity get destroyed, i.e., the data will not get downloaded
 * again, if the screen orientation change.
 */
public class MovieDetailsLoader extends AsyncTaskLoader<Movie> {

    private String requestDetailsUrl;
    private String requestReviewsUrl;
    private String requestSimilarMoviesUrl;

    public MovieDetailsLoader(Context context, String movieDetailsUrl,
                              String movieReviewsUrl, String similarMoviesUrl) {
        super(context);
        requestDetailsUrl = movieDetailsUrl;
        requestReviewsUrl = movieReviewsUrl;
        requestSimilarMoviesUrl = similarMoviesUrl;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public Movie loadInBackground() {

        if (requestDetailsUrl == null) {
            return null;
        }

        return ExtractMoviesData.extractMovieDetails(requestDetailsUrl, requestReviewsUrl, requestSimilarMoviesUrl);
    }
}
