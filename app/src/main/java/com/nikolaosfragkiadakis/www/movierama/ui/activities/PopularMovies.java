package com.nikolaosfragkiadakis.www.movierama.ui.activities;

/* MIT License

Copyright 2020 © Nikolaos Fragkiadakis / www.nikolaosfragkiadakis.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

import android.app.LoaderManager;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.nikolaosfragkiadakis.www.movierama.uiadapters.MoviesAdapter;
import com.nikolaosfragkiadakis.www.movierama.dataloaders.MoviesLoader;
import com.nikolaosfragkiadakis.www.movierama.datamodel.dataset.Movie;
import com.nikolaosfragkiadakis.www.movierama.R;
import com.nikolaosfragkiadakis.www.movierama.utilities.Constants.PopularMoviesConstants;
import com.nikolaosfragkiadakis.www.movierama.infinitescroll.InfiniteScrollListener;

import java.util.ArrayList;

import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;
import com.nikolaosfragkiadakis.www.movierama.utilities.NetworkCheck;

/**
 * The {@link PopularMovies} is the home screen of the Movie Rama app,
 * which provides the UI with the current popular movies' data from the TMDB JSON API.
 */
public class PopularMovies extends AppCompatActivity implements LoaderCallbacks<ArrayList<Movie>>, MoviesAdapter.OnMovieListener {
    // The final composed URL for loading from the TMDB JSON API.
    private static String finalLoadMoreMoviesRequestURL;

    // The reference to the LoaderManager.
    private LoaderManager mLoaderManager;

    // The adapter for the movies' list.
    private MoviesAdapter mAdapter;

    // The recycler view for the movies' list.
    private RecyclerView mRecyclerView;

    // The reference to the InfiniteScrollListener.
    private InfiniteScrollListener mRecyclerViewInfiniteScrollListener;

    // The reference to the SwipeRefreshLayout.
    private SwipeRefreshLayout mSwipeRefreshLayout;

    // The reference to the empty state TextView.
    private TextView emptyStateTextView;

    // The reference to the progressBar.
    private ProgressBar progressBar;

    // The reference to the toast message.
    private Toast toast;

    // The reference to the Intent instance.
    private Intent intent;

    // The local reference of the new page number.
    private int currentPageNumber = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_screen);

        AppCenter.start(getApplication(), "eaaffa2c-9466-44ab-a285-1fa1c2ad650a",
                Analytics.class, Crashes.class);

        // Set the visible title of the Popular Movies activity programmatically.
        // The "label" attribute in the AndroidManifest.xml file confused the
        // android platform and was getting the place of the App's name ("Movie Rama").
        this.setTitle(R.string.popular_movies_activity_name);

        if (NetworkCheck.isNetworkConnected(PopularMovies.this)) {
            // There is an internet connection, thus the movies' data can get downloaded.
            initialize();
        } else {
            // There's no internet connection. Hide the loading indicator because
            // the data cannot be loaded.
            progressBar = findViewById(R.id.progress_bar);
            progressBar.setVisibility(ProgressBar.GONE);

            // Show an appropriate message in the UI.
            emptyStateTextView = findViewById(R.id.empty_view);
            emptyStateTextView.setText(R.string.no_internet_connection);
        }

        // Find a reference to the SearchView in the layout.
        SearchView mSearchView = findViewById(R.id.search_view);

        // Set an "OnQueryTextListener" in order a potential query from the user could get caught.
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                submitUserQuery(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        // Find a reference to the SwipeRefreshLayout in the layout.
        mSwipeRefreshLayout = findViewById(R.id.swipe_to_refresh);

        // Set refresh indicator's color.
        mSwipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent),
                getResources().getColor(R.color.colorPrimary));

        // Set an "OnRefreshListener" in order to give at the user the functionality
        // to perform a swipe-to-refresh gesture.
        // Refresh the UI.
        mSwipeRefreshLayout.setOnRefreshListener(this::refresh);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu.
        if (item.getItemId() == R.id.more_information) {
            // Open the "MoreInformation" activity.
            intent = new Intent(this, MoreInformation.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Save the current state of the PopularMovieActivity.
        SharedPreferences prefs = getSharedPreferences(PopularMoviesConstants.PREF_ACTIVITY_NAME_FIELD_TAG, MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PopularMoviesConstants.PREF_LAST_ACTIVITY_FIELD_TAG, getClass().getName());
        editor.apply();
    }

    /**
     * Set up an Async Task Loader in order to perform the HTTP Request.
     */
    @Override
    public Loader<ArrayList<Movie>> onCreateLoader(int i, Bundle bundle) {
        // Create a new loader for the given URL.
        if (i == 1) {
            return new MoviesLoader(PopularMovies.this, PopularMoviesConstants.POPULAR_MOVIES_REQUEST_URL);
        } else {
            return new MoviesLoader(PopularMovies.this, finalLoadMoreMoviesRequestURL);
        }
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<Movie>> loader, ArrayList<Movie> movies) {
        // Hide loading indicator because the data has been loaded.
        progressBar = findViewById(R.id.progress_bar);
        progressBar.setVisibility(ProgressBar.GONE);

        // If there is no parsed data, the Empty View state text must be showed.
        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setVisibility(View.GONE);
        emptyStateTextView = findViewById(R.id.empty_view);
        emptyStateTextView.setText(R.string.no_data_found);

        // If the data have successfully been parsed, update the UI.
        if (movies != null && !movies.isEmpty() && mAdapter.getItemCount() == 0) {
            // This is the first load.
            mRecyclerView.setVisibility(View.VISIBLE);
            emptyStateTextView.setVisibility(View.GONE);
            mAdapter = new MoviesAdapter(this, movies, this);
            mRecyclerView.setAdapter(mAdapter);
        } else if (movies != null && !movies.isEmpty()) {
            // These are the following loads.
            mRecyclerView.setVisibility(View.VISIBLE);
            emptyStateTextView.setVisibility(View.GONE);
            mAdapter.addMovies(movies);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<Movie>> loader) {
        // Clear the existing data set and notify the adapter for the change.
        if (NetworkCheck.isNetworkConnected(PopularMovies.this)) {
            mAdapter.getMovies().clear();
            mAdapter.notifyDataSetChanged();

            // Rest the "InfiniteScrollListener" for performing a new search.
            mRecyclerViewInfiniteScrollListener.resetState();
        }
    }

    /**
     * Initialize the {@link PopularMovies}.
     */
    public void initialize() {
        // Find a reference to the RecyclerView in the layout.
        mRecyclerView = findViewById(R.id.recycler_view);

        // Create a new ArrayAdapter of movies.
        mAdapter = new MoviesAdapter(this, new ArrayList<>(), this);

        // Set the layout manager on the RecyclerView and specify how the data set
        // will be displayed in the UI.
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        // Set the adapter on the RecyclerView so the list can be populated in the UI.
        mRecyclerView.setAdapter(mAdapter);

        // Get a reference to the LoaderManager, in order to interact with loaders.
        mLoaderManager = getLoaderManager();

        // Initialize the loader. Pass in the int ID constant defined above and pass in null for
        // the bundle. Pass in this activity for the LoaderCallbacks parameter ( which is valid
        // because this activity implements the LoaderCallbacks interface ).
        mLoaderManager.initLoader(PopularMoviesConstants.MOVIES_LOADER_ID, null, this).forceLoad();

        // Set the InfiniteScrollListener to the RecyclerView.
        mRecyclerViewInfiniteScrollListener = new InfiniteScrollListener(mLinearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadMoreMovies(page);
            }
        };

        mRecyclerView.addOnScrollListener(mRecyclerViewInfiniteScrollListener);
    }

    /**
     * Triggered from the InfiniteScrollListener in order to load more movies' data.
     *
     * @param newPageNumber represents the number of the new page of movies data from the TMDB JSON API to get loaded.
     */
    public void loadMoreMovies(int newPageNumber) {
        // Update the local instance of the new page number.
        currentPageNumber = newPageNumber;

        if (NetworkCheck.isNetworkConnected(PopularMovies.this)) {
            // There is an internet connection, thus the movies' data can get loaded.

            // Show loading indicator till the data has been loaded.
            progressBar = findViewById(R.id.progress_bar);
            progressBar.setVisibility(ProgressBar.VISIBLE);

            // Create the fully composed load more movies' request URL by concatenate its components.
            finalLoadMoreMoviesRequestURL = PopularMoviesConstants.LOAD_MORE_MOVIES_REQUEST_URL + newPageNumber;

            // Get a reference to the LoaderManager, in order to interact with loaders.
            mLoaderManager = getLoaderManager();

            // Restart the Movies Loader.
            mLoaderManager.restartLoader(PopularMoviesConstants.MORE_MOVIES_LOADER_ID, null, this).forceLoad();
        } else {
            // There is no internet connection, thus the movies' data cannot get loaded.
            // Show an appropriate message in the UI.
            toast = Toast.makeText(this, R.string.error_during_loading_more_movies, Toast.LENGTH_LONG);
            toast.show();
        }
    }

    /**
     * The user has perform a swipe-to-refresh gesture so the UI should get updated.
     */
    public void refresh() {
        // Check the internet connection status of the device.
        boolean isConnected = NetworkCheck.isNetworkConnected(PopularMovies.this);

        if (isConnected && currentPageNumber == 0) {
            // There is an internet connection, thus the movies' data can get refreshed, but
            // the activity has been launched initially in no internet connection mode,
            // so it must get initialized first in order to prevent a crash.
            emptyStateTextView = findViewById(R.id.empty_view);
            emptyStateTextView.setVisibility(View.GONE);

            // Show loading indicator till the data has been loaded.
            progressBar = findViewById(R.id.progress_bar);
            progressBar.setVisibility(ProgressBar.VISIBLE);

            // Initialize the PopularMovies.
            initialize();

            // Show an appropriate message in the UI.
            toast = Toast.makeText(this, R.string.popular_movies_refresh_message, Toast.LENGTH_LONG);
            toast.show();

            // Hide the "refresh" indicator.
            mSwipeRefreshLayout.setRefreshing(false);
        } else if (isConnected) {
            // There is an internet connection, thus the movies' data can get refreshed.
            emptyStateTextView = findViewById(R.id.empty_view);
            emptyStateTextView.setVisibility(View.GONE);

            // Show loading indicator till the data has been loaded.
            progressBar = findViewById(R.id.progress_bar);
            progressBar.setVisibility(ProgressBar.VISIBLE);

            // Clear the existing data set and notify the adapter for the change.
            mAdapter.getMovies().clear();
            mAdapter.notifyDataSetChanged();

            // Rest the "InfiniteScrollListener" for performing a new search.
            mRecyclerViewInfiniteScrollListener.resetState();

            // Get a reference to the LoaderManager, in order to interact with loaders.
            mLoaderManager = getLoaderManager();

            // Destroy the Loaders in order to able to get initialized again.
            mLoaderManager.destroyLoader(PopularMoviesConstants.MOVIES_LOADER_ID);
            mLoaderManager.destroyLoader(PopularMoviesConstants.MORE_MOVIES_LOADER_ID);

            // Initialize again the Movies Loader.
            mLoaderManager.initLoader(PopularMoviesConstants.MOVIES_LOADER_ID, null, PopularMovies.this).forceLoad();

            // Show an appropriate message in the UI.
            toast = Toast.makeText(this, R.string.popular_movies_refresh_message, Toast.LENGTH_LONG);
            toast.show();

            // Hide the "refresh" indicator.
            mSwipeRefreshLayout.setRefreshing(false);
        } else {
            // There's no internet connection, the data cannot be loaded.
            // Show an appropriate message in the UI.
            mRecyclerView = findViewById(R.id.recycler_view);
            mRecyclerView.setVisibility(View.GONE);
            emptyStateTextView = findViewById(R.id.empty_view);
            emptyStateTextView.setVisibility(View.VISIBLE);
            emptyStateTextView.setText(R.string.no_internet_connection);

            // Hide the "swipe to refresh layout" loading indicator.
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    /**
     * In the case that a movies' expert wants to search for a movie by title, then the query must get submitted.
     *
     * @param query represents the user's query.
     */
    public void submitUserQuery(String query) {
        if (NetworkCheck.isNetworkConnected(PopularMovies.this)) {
            // There is an internet connection, thus the user's search query can get submitted.
            intent = new Intent(this, SearchResults.class);
            intent.putExtra(PopularMoviesConstants.EXTRA_USER_QUERY_FIELD_TAG, query);
            startActivity(intent);
        } else {
            // There is no internet connection, thus the user's search query cannot get submitted.
            // Show an appropriate message in the UI.
            toast = Toast.makeText(this, R.string.error_during_submitting_search_query, Toast.LENGTH_LONG);
            toast.show();
        }
    }

    /**
     * In the case that a curious user wants to know more about a movie, then its details must be shown.
     *
     * @param position represents the position of the clicked movie.
     */
    @Override
    public void onMovieClick(int position) {
        if (NetworkCheck.isNetworkConnected(PopularMovies.this)) {
            // There is an internet connection, thus the movie's data can get loaded.
            intent = new Intent(this, MovieDetails.class);
            intent.putExtra(PopularMoviesConstants.EXTRA_MOVIE_ID_FIELD_TAG, mAdapter.getMovie(position).getID());
            startActivity(intent);
        } else {
            // There is no internet connection, thus the movie's data cannot get loaded.
            // Show an appropriate message in the UI.
            toast = Toast.makeText(this, R.string.error_during_loading_single_movie_data, Toast.LENGTH_LONG);
            toast.show();
        }
    }
}
