package com.nikolaosfragkiadakis.www.movierama.ui.activities;

/* MIT License

Copyright 2020 © Nikolaos Fragkiadakis / www.nikolaosfragkiadakis.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nikolaosfragkiadakis.www.movierama.uiadapters.ReviewsAdapter;
import com.nikolaosfragkiadakis.www.movierama.uiadapters.SimilarMoviesAdapter;
import com.nikolaosfragkiadakis.www.movierama.dataloaders.MovieDetailsLoader;
import com.nikolaosfragkiadakis.www.movierama.datamodel.dataset.Movie;
import com.nikolaosfragkiadakis.www.movierama.datamodel.datapersistence.MovieContract.MovieEntry;
import com.nikolaosfragkiadakis.www.movierama.R;
import com.nikolaosfragkiadakis.www.movierama.utilities.Constants.MovieDetailsConstants;
import com.nikolaosfragkiadakis.www.movierama.utilities.Constants.ExtractDataConstants;
import com.nikolaosfragkiadakis.www.movierama.utilities.NetworkCheck;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * The {@link MovieDetails} is the movie details screen of the Movie Rama app,
 * which provides the UI with the clicked movie's data from the TMDB JSON API.
 */
public class MovieDetails extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Movie>, SimilarMoviesAdapter.OnSimilarMovieListener {
    // The movie's details fully composed request URL from the TMDB JSON API.
    private String movieDetailsRequestUrl;

    // The movie's review fully composed request URL from the TMDB JSON API.
    private String movieReviewListRequestUrl;

    // The movie's similar movies list fully composed request URL from the TMDB JSON API.
    private String movieSimilarMoviesListRequestUrl;

    // The container of the movie's details data in the UI.
    private NestedScrollView mScrollView;

    // The reference to the progressBar.
    private ProgressBar progressBar;

    // The reference to the empty view TextView.
    private TextView emptyStateTextView;

    // The reference to the current movie's data set.
    private Movie mMovie;

    // The reference to the title TextView.
    private TextView titleTextView;

    // The reference to the genre TextView.
    private TextView genreTextView;

    // The reference to the release date TextView.
    private TextView releaseDateTextView;

    // The reference to the rating ratingBar.
    private RatingBar ratingBar;

    // The reference to the description TextView.
    private TextView descriptionTextView;

    // The reference to the director TextView.
    private TextView directorTextView;

    // The reference to the cast TextView.
    private TextView castTextView;

    // The reference to the poster ImageView.
    private ImageView posterImageView;

    // The reference to the favorite ImageView.
    private ImageView mFavoriteImageView;

    // The adapter for the reviews' list.
    private ReviewsAdapter mReviewsAdapter;

    // The adapter for the similar movies' list.
    private SimilarMoviesAdapter mSimilarMoviesAdapter;

    // The reference to the LoaderManager.
    private LoaderManager mLoaderManager;

    // The recycler view for the reviews' list.
    private RecyclerView mReviewsRecyclerView;

    // The recycler view for the similar movies's list.
    private RecyclerView mSimilarMoviesRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie_details);

        // Set the visible title of the Popular Movies activity programmatically.
        this.setTitle(R.string.movie_details_activity_name);

        // Create the fully composed movie's details and movie's reviews request URLs by concatenate their components.
        Intent intent = getIntent();
        int clickedMovieID = intent.getIntExtra(MovieDetailsConstants.EXTRA_MOVIE_ID_FIELD_TAG, -1);
        if (clickedMovieID != -1) {
            composeTheFinalUrlStrings(clickedMovieID);
        }

        if (NetworkCheck.isNetworkConnected(MovieDetails.this)) {
            // There is an internet connection, thus the movie's data can get downloaded.

            // Find a reference to the ScrollView in the layout.
            mScrollView = findViewById(R.id.nested_scroll_view);
            mScrollView.setVisibility(View.GONE);

            // Find a reference to the "reviews_recycler_view" RecyclerView in the layout.
            mReviewsRecyclerView = findViewById(R.id.reviews_recycler_view);

            // Create a new ArrayAdapter of reviews.
            mReviewsAdapter = new ReviewsAdapter(this, new ArrayList<>());

            // Set the layout manager on the RecyclerView and specify how the data set
            // will be displayed in the UI.
            LinearLayoutManager mReviewsLinearLayoutManager = new LinearLayoutManager(this);
            mReviewsRecyclerView.setLayoutManager(mReviewsLinearLayoutManager);

            // Set the adapter on the "reviews_recycler_view" RecyclerView so the list can be populated in the UI.
            mReviewsRecyclerView.setAdapter(mReviewsAdapter);

            // Find a reference to the "similar_movies_recycler_view" RecyclerView in the layout.
            mSimilarMoviesRecyclerView = findViewById(R.id.similar_movies_recycler_view);

            // Create a new ArrayAdapter of reviews.
            mSimilarMoviesAdapter = new SimilarMoviesAdapter(this, new ArrayList<>(), this);

            // Set the layout manager on the RecyclerView and specify how the data set
            // will be displayed in the UI.
            LinearLayoutManager mSimilarMoviesLinearLayoutManager = new LinearLayoutManager(this);
            mSimilarMoviesLinearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
            mSimilarMoviesRecyclerView.setLayoutManager(mSimilarMoviesLinearLayoutManager);

            // Set the adapter on the "reviews_recycler_view" RecyclerView so the list can be populated in the UI.
            mSimilarMoviesRecyclerView.setAdapter(mSimilarMoviesAdapter);

            // Get a reference to the LoaderManager, in order to interact with loaders.
            mLoaderManager = getLoaderManager();

            // Initialize the Movie Details Loader. Pass in the int ID constant defined above and pass in null for
            // the bundle. Pass in this activity for the LoaderCallbacks parameter ( which is valid
            // because this activity implements the LoaderCallbacks interface ).
            mLoaderManager.initLoader(MovieDetailsConstants.MOVIE_DETAILS_LOADER_ID, null, this).forceLoad();

            // Attach an "OnClickListener" in the favorite button.
            mFavoriteImageView = findViewById(R.id.single_movie_favorite);

            mFavoriteImageView.setOnClickListener(view -> {
                if (!MovieEntry.isFavorite(MovieDetails.this, mMovie.getID())) {
                    MovieEntry.addFavorite(MovieDetails.this, mMovie.getID());
                    mFavoriteImageView.setImageResource(R.drawable.ic_favorite_filled);
                    Toast.makeText(MovieDetails.this, R.string.favorite_added, Toast.LENGTH_LONG).show();
                } else {
                    MovieEntry.removeFavorite(MovieDetails.this, mMovie.getID());
                    mFavoriteImageView.setImageResource(R.drawable.ic_favorite_outline);
                    Toast.makeText(MovieDetails.this, R.string.favorite_removed, Toast.LENGTH_LONG).show();
                }
            });

        } else {
            // There's no internet connection. Hide the loading indicator because
            // the data cannot get loaded.
            progressBar = findViewById(R.id.single_movie_progress_bar);
            progressBar.setVisibility(ProgressBar.GONE);

            // Hide data container and show an appropriate message in the UI.
            mScrollView = findViewById(R.id.nested_scroll_view);
            mScrollView.setVisibility(View.GONE);
            emptyStateTextView = findViewById(R.id.single_movie_empty_view);
            emptyStateTextView.setText(R.string.no_internet_connection);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu.
        if (item.getItemId() == android.R.id.home) {
            // Respond to a click on the "Up" arrow button in the app bar.
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // User clicked on the back button.
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Save the current state of the MovieDetails.
        SharedPreferences prefs = getSharedPreferences(MovieDetailsConstants.PREF_ACTIVITY_NAME_FIELD_TAG, MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(MovieDetailsConstants.PREF_LAST_ACTIVITY_FIELD_TAG, getClass().getName());
        editor.apply();
    }

    /**
     * Set up an Async Task Loader in order to perform the HTTP Request.
     */
    @Override
    public Loader<Movie> onCreateLoader(int i, Bundle bundle) {
        // Create a new loader for the given URL.
        return new MovieDetailsLoader(MovieDetails.this,
                movieDetailsRequestUrl,
                movieReviewListRequestUrl,
                movieSimilarMoviesListRequestUrl);
    }

    @Override
    public void onLoadFinished(Loader<Movie> loader, Movie movie) {
        // Hide loading indicator because the data has been loaded.
        progressBar = findViewById(R.id.single_movie_progress_bar);
        progressBar.setVisibility(ProgressBar.GONE);

        // If there is no parsed data, the Empty View state text must be showed.
        mScrollView = findViewById(R.id.nested_scroll_view);
        mScrollView.setVisibility(View.GONE);
        emptyStateTextView = findViewById(R.id.single_movie_empty_view);
        emptyStateTextView.setText(R.string.no_data_found);

        // If the data have successfully been parsed, update the UI.
        if (movie != null) {
            mScrollView = findViewById(R.id.nested_scroll_view);
            mScrollView.setVisibility(View.VISIBLE);
            emptyStateTextView = findViewById(R.id.single_movie_empty_view);
            emptyStateTextView.setVisibility(View.GONE);
            showMovieDetails(movie);
        }
    }

    @Override
    public void onLoaderReset(Loader<Movie> loader) {
        // Clear the existing data set and notify the reviews adapter for the change.
        if (NetworkCheck.isNetworkConnected(MovieDetails.this)) {
            titleTextView.setText("");
            genreTextView.setText("");
            releaseDateTextView.setText("");
            ratingBar.setRating((float) 0.0);
            descriptionTextView.setText("");
            directorTextView.setText("");
            castTextView.setText("");
            posterImageView.setImageResource(R.drawable.ic_default_poster);
            mFavoriteImageView.setImageResource(R.drawable.ic_favorite_outline);
            mReviewsAdapter.getReviews().clear();
            mReviewsAdapter.notifyDataSetChanged();
            mSimilarMoviesAdapter.getMovies().clear();
            mSimilarMoviesAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Compose the necessary final url strings for the "MovieDetailsLoader".
     *
     * @param movieId the clicked movie id.
     */
    public void composeTheFinalUrlStrings(int movieId) {
        movieDetailsRequestUrl = MovieDetailsConstants.PREFIX_OF_MOVIE_DETAILS_REQUEST_URL +
                movieId + MovieDetailsConstants.SUPPLEMENTARY_QUERY_OF_MOVIE_DETAILS_REQUEST_URL;

        movieReviewListRequestUrl = MovieDetailsConstants.PREFIX_OF_REVIEWS_REQUEST_URL +
                movieId + MovieDetailsConstants.SUPPLEMENTARY_QUERY_OF_REVIEWS_REQUEST_URL;

        movieSimilarMoviesListRequestUrl = MovieDetailsConstants.PREFIX_OF_SIMILAR_MOVIES_REQUEST_URL +
                movieId + MovieDetailsConstants.SUPPLEMENTARY_QUERY_OF_SIMILAR_MOVIES_REQUEST_URL;
    }

    /**
     * Show the movie's details in the UI.
     *
     * @param movie represents the clicked movie's data.
     */
    public void showMovieDetails(Movie movie) {
        // Update the reference to the current movie's data set.
        mMovie = movie;

        titleTextView = findViewById(R.id.single_movie_title);
        titleTextView.setText(mMovie.getTitle());

        genreTextView = findViewById(R.id.single_movie_genre);
        if (mMovie.getGenres().equals(ExtractDataConstants.EMPTY_STRING_TAG)) {
            genreTextView.setText(R.string.no_genres_provided);
        } else {
            genreTextView.setText(mMovie.getGenres());
        }

        releaseDateTextView = findViewById(R.id.single_movie_release_date);
        if (mMovie.getReleaseDate().equals(ExtractDataConstants.EMPTY_STRING_TAG)) {
            releaseDateTextView.setText(R.string.no_release_date_provided);
        } else {
            releaseDateTextView.setText(mMovie.getReleaseDate());
        }

        ratingBar = findViewById(R.id.single_movie_rating);
        ratingBar.setRating(mMovie.getRatingNumber());

        descriptionTextView = findViewById(R.id.movie_description);
        if (mMovie.getDescription().equals(ExtractDataConstants.EMPTY_STRING_TAG)) {
            descriptionTextView.setText(R.string.no_description_provided);
        } else {
            descriptionTextView.setText(mMovie.getDescription());
        }

        directorTextView = findViewById(R.id.movie_director);
        if (mMovie.getDirector().equals(ExtractDataConstants.EMPTY_STRING_TAG)) {
            directorTextView.setText(R.string.unknown);
        } else {
            directorTextView.setText(mMovie.getDirector());
        }

        castTextView = findViewById(R.id.movie_cast);
        if (mMovie.getCast().equals(ExtractDataConstants.EMPTY_STRING_TAG)) {
            castTextView.setText(R.string.unknown);
        } else {
            castTextView.setText(mMovie.getCast());
        }

        // Figure out if there are reviews for the current movie and
        // update the UI accordingly.
        TextView mReviewsListTitleTextView = findViewById(R.id.reviews_header);
        if (mMovie.getReviewList().isEmpty()) {
            mReviewsListTitleTextView.setVisibility(View.GONE);
            mReviewsRecyclerView.setVisibility(View.GONE);
        } else {
            mReviewsListTitleTextView.setVisibility(View.VISIBLE);
            mReviewsRecyclerView.setVisibility(View.VISIBLE);
            mReviewsAdapter = new ReviewsAdapter(this, mMovie.getReviewList());
            mReviewsRecyclerView.setAdapter(mReviewsAdapter);
        }

        // Figure out if there are similar movies for the current movie and
        // update the UI accordingly.
        TextView mSimilarMoviesTitleTextView = findViewById(R.id.similar_movies_header);
        ImageView mSimilarMoviesNavigationArrows = findViewById(R.id.similar_movies_navigation_arrows);
        if (mMovie.getSimilarMoviesList().isEmpty()) {
            mSimilarMoviesTitleTextView.setVisibility(View.GONE);
            mSimilarMoviesRecyclerView.setVisibility(View.GONE);
            mSimilarMoviesNavigationArrows.setVisibility(View.GONE);
        } else {
            mSimilarMoviesTitleTextView.setVisibility(View.VISIBLE);
            mSimilarMoviesRecyclerView.setVisibility(View.VISIBLE);
            mSimilarMoviesNavigationArrows.setVisibility(View.VISIBLE);
            mSimilarMoviesAdapter = new SimilarMoviesAdapter(this, mMovie.getSimilarMoviesList(), this);
            mSimilarMoviesRecyclerView.setAdapter(mSimilarMoviesAdapter);
        }

        // Download the movie's poster using the "Picasso" library
        // in a attempt to strive for better performance.
        posterImageView = findViewById(R.id.single_movie_poster);
        Picasso.get().load(MovieDetailsConstants.PREFIX_OF_POSTER_REQUEST_URL +
                mMovie.getPosterPath()).into(posterImageView);

        // Try to figure out if the current movie has been added to the favorite
        // list by the user and update the UI properly.
        mFavoriteImageView = findViewById(R.id.single_movie_favorite);
        if (MovieEntry.isFavorite(this, mMovie.getID())) {
            mFavoriteImageView.setImageResource(R.drawable.ic_favorite_filled);
        } else {
            mFavoriteImageView.setImageResource(R.drawable.ic_favorite_outline);
        }
    }

    /**
     * If a curious user put his/her finger on a similar movie, that movie's details should get loaded.
     *
     * @param position represents the selected similar movie current position.
     */
    @Override
    public void onSimilarMovieClick(int position) {
        if (NetworkCheck.isNetworkConnected(MovieDetails.this)) {
            // There is an internet connection, thus the movie's data can get loaded.
            Movie clickedSimilarMovie = mSimilarMoviesAdapter.getMovie(position);

            // Hide the empty view and show loading indicator till the data has been loaded.
            progressBar = findViewById(R.id.single_movie_progress_bar);
            progressBar.setVisibility(ProgressBar.VISIBLE);
            mScrollView = findViewById(R.id.nested_scroll_view);
            mScrollView.setVisibility(View.GONE);


            //TODO: Scroll on the top of the screen properly.
            //mScrollView.fullScroll(View.FOCUS_UP);
            //mScrollView.fullScroll(View.FOCUS_UP);
            //mScrollView.fling(0);
            //mScrollView.scrollTo(mScrollView.getChildAt(0).getTop(),mScrollView.getChildAt(0).getTop());


            emptyStateTextView = findViewById(R.id.single_movie_empty_view);
            emptyStateTextView.setVisibility(View.GONE);

            // Clear the existing data set and notify the adapter for the change.
            mReviewsAdapter.getReviews().clear();
            mReviewsAdapter.notifyDataSetChanged();
            mSimilarMoviesAdapter.getMovies().clear();
            mSimilarMoviesAdapter.notifyDataSetChanged();

            // Compose the new final url strings for the "MovieDetailsLoader".
            if (clickedSimilarMovie.getID() != -1) {
                composeTheFinalUrlStrings(clickedSimilarMovie.getID());
            }

            // Get a reference to the LoaderManager, in order to interact with loaders.
            mLoaderManager = getLoaderManager();

            // Destroy the LoaderManager in order to able to get initialized again.
            mLoaderManager.destroyLoader(MovieDetailsConstants.MOVIE_DETAILS_LOADER_ID);

            // Initialize the Movie Details Loader again.
            mLoaderManager.initLoader(MovieDetailsConstants.MOVIE_DETAILS_LOADER_ID, null, this).forceLoad();
        } else {
            // There is no internet connection, thus the movie's data cannot get loaded.
            // Show an appropriate message in the UI.
            Toast.makeText(this, R.string.error_during_loading_single_movie_data, Toast.LENGTH_LONG).show();
        }
    }
}
