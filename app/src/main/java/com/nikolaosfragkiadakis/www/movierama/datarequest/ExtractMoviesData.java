package com.nikolaosfragkiadakis.www.movierama.datarequest;

/* MIT License

Copyright 2020 © Nikolaos Fragkiadakis / www.nikolaosfragkiadakis.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import com.nikolaosfragkiadakis.www.movierama.datamodel.dataset.Movie;
import com.nikolaosfragkiadakis.www.movierama.datamodel.dataset.Review;
import com.nikolaosfragkiadakis.www.movierama.utilities.Constants.ExtractDataConstants;
import com.nikolaosfragkiadakis.www.movierama.utilities.Constants.TmdbApiConstants;

/**
 * This class is a container of all the helper methods that are related to requesting and receiving
 * the movies' data from the TMDB JSON API.
 */
public class ExtractMoviesData {

    /**
     * Create a private constructor because {@link ExtractMoviesData} class is only meant to hold
     * static variables and methods, which can be accessed directly from the class name
     * {@link ExtractMoviesData}, thus an object instance of ExtractMoviesData is not needed.
     */
    private ExtractMoviesData() {
    }

    /////////////////////////////////////////////////////
    ////////////// Extract Data Utilities ///////////////
    /////////////////////////////////////////////////////

    /**
     * Return a list of {@link Movie} objects that has been built up from parsing the given
     * JSON response.
     */
    public static ArrayList<Movie> extractMovies(String requestUrl) {

        // Try to perform an HTTP request to the TMDB endpoint URL and receive a JSON response back.
        // In case of an error, print a message to the logs.
        String jsonResponse = null;
        try {
            jsonResponse = MoviesHttpRequest.perform(requestUrl);
        } catch (IOException | NullPointerException e) {
            Log.e(ExtractDataConstants.ERROR_LOG_TAG, ExtractDataConstants.ERROR_LOG_MESSAGE, e);
        }

        // Create an empty List that we can start adding movies to.
        ArrayList<Movie> movies = new ArrayList<>();

        // Try to parse the JSON response string. If there's a problem with the way the JSON
        // is formatted, catch the JSONException exception object that will be thrown so the
        // app doesn't crash, and print the error message to the logs.
        try {
            if (jsonResponse != null) {
                // Convert the JSON response String into a JSONObject.
                JSONObject jsonRootObject = new JSONObject(jsonResponse);

                // Get the instance of the "results" JSONArray which contains the movie objects.
                JSONArray resultsArray = jsonRootObject.optJSONArray(TmdbApiConstants.MOVIES_RESULTS);

                if (resultsArray != null) {
                    // Loop through each movie object in the "results" array.
                    for (int i = 0; i < resultsArray.length(); i++) {
                        // Get the movie JSONObject at position i.
                        JSONObject movieObject = resultsArray.getJSONObject(i);

                        // 1. Extract "id" for movie's id.
                        // 2. Extract "poster_path" for movie's title.
                        // 3. Extract "title" for movie's title.
                        // 4. Extract "release_date" for movie's release date.
                        // 5. Extract "vote_average" for movie's rating number.
                        int id = movieObject.optInt(TmdbApiConstants.MOVIE_ID);
                        String posterPath = movieObject.optString(TmdbApiConstants.MOVIE_POSTER_PATH);
                        String title = movieObject.optString(TmdbApiConstants.MOVIE_TITLE);
                        String releaseDate = movieObject.optString(TmdbApiConstants.MOVIE_RELEASE_DATE);
                        Long rating = movieObject.optLong(TmdbApiConstants.MOVIE_VOTE_AVERAGE);

                        // Create a new Movie instance from the extracted data.
                        Movie movie = new Movie.Builder()
                                .setId(id)
                                .setPosterPath(posterPath)
                                .setTitle(title)
                                .setReleaseDate(releaseDate)
                                .setRatingNumber(rating)
                                .build();

                        // Add the movie to the list of movies.
                        movies.add(movie);
                    }
                }
            }
        } catch (JSONException | NullPointerException e) {
            Log.e(ExtractDataConstants.ERROR_LOG_TAG, ExtractDataConstants.ERROR_LOG_MESSAGE, e);
        }

        // Finally return the list of movies.
        return movies;
    }

    /**
     * Return a {@link Movie} object that has been built up from parsing the given
     * JSON response.
     */
    public static Movie extractMovieDetails(String movieDetailsRequestUrl,
                                            String movieReviewsRequestUrl,
                                            String movieSimilarMoviesRequestUrl) {

        // Try to perform an HTTP request to the TMDB endpoints URLs and receive a JSON response back.
        // In case of an error, print a message to the logs.
        String jsonResponseForMovieDetails = null;
        try {
            jsonResponseForMovieDetails = MoviesHttpRequest.perform(movieDetailsRequestUrl);
        } catch (IOException e) {
            Log.e(ExtractDataConstants.ERROR_LOG_TAG, ExtractDataConstants.ERROR_LOG_MESSAGE, e);
        }

        // The reference to the Movie instance that will store the requested movie's details.
        Movie movie = null;

        // Try to parse the JSON response string. If there's a problem with the way the JSON
        // is formatted, catch the JSONException exception object that will be thrown so the
        // app doesn't crash, and print the error message to the logs.
        try {
            if (jsonResponseForMovieDetails != null) {
                // Convert the JSON response String for the movie into a JSONObject.
                JSONObject jsonRootMovieObject = new JSONObject(jsonResponseForMovieDetails);

                // 1. Extract "id" for movies's id.
                // 2. Extract "poster_path" for movie's poster path.
                // 3. Extract "title" for movie's title.
                // 4. Extract "genres" for movie's genres.
                // 5. Extract "release_date" for movie's release date.
                // 6. Extract "vote_average" for movie's rating number.
                // 7. Extract "overview" for movie's description.
                // 8. Extract "name" from "crew" for movie's director.
                // 9. Extract "name" from "cast" for movie's cast.
                int id = jsonRootMovieObject.optInt(TmdbApiConstants.MOVIE_ID);
                String posterPath = jsonRootMovieObject.optString(TmdbApiConstants.MOVIE_POSTER_PATH);
                String title = jsonRootMovieObject.optString(TmdbApiConstants.MOVIE_TITLE);

                String genres = ExtractDataConstants.EMPTY_STRING_TAG;
                JSONArray genresArray = jsonRootMovieObject.optJSONArray(TmdbApiConstants.MOVIE_GENRES);
                if (genresArray != null) {
                    for (int i = 0; i < genresArray.length(); i++) {
                        JSONObject genreCategoryObject = genresArray.getJSONObject(i);
                        String genreName = genreCategoryObject.optString(TmdbApiConstants.MOVIE_GENRE_NAME);
                        genres = genres + genreName + ExtractDataConstants.WORDS_SEPARATOR + ExtractDataConstants.SPACE_CHARACTER;
                    }
                    // Remove the words separator at the end the genres string.
                    int genresIndex = genres.lastIndexOf(ExtractDataConstants.WORDS_SEPARATOR);
                    if (genresIndex != -1) {
                        genres = genres.substring(0, genresIndex) + genres.substring(genresIndex + 1);
                    }
                }

                String releaseDate = jsonRootMovieObject.optString(TmdbApiConstants.MOVIE_RELEASE_DATE);
                Long rating = jsonRootMovieObject.optLong(TmdbApiConstants.MOVIE_VOTE_AVERAGE);
                String description = jsonRootMovieObject.optString(TmdbApiConstants.MOVIE_OVERVIEW);

                String director = ExtractDataConstants.EMPTY_STRING_TAG;
                String cast = ExtractDataConstants.EMPTY_STRING_TAG;
                JSONObject creditsObject = jsonRootMovieObject.optJSONObject(TmdbApiConstants.MOVIE_CREDITS);
                if (creditsObject != null) {

                    JSONArray crewArray = creditsObject.getJSONArray(TmdbApiConstants.MOVIE_CREW);
                    int z = 0;
                    boolean foundDirector = false;
                    while (z < crewArray.length() && !foundDirector) {
                        JSONObject crewMemberObject = crewArray.getJSONObject(z);
                        if (crewMemberObject.optString(TmdbApiConstants.MOVIE_CREW_JOB).equals(TmdbApiConstants.MOVIE_DIRECTOR)) {
                            director = crewMemberObject.optString(TmdbApiConstants.MOVIE_DIRECTOR_NAME);
                            foundDirector = true;
                        } else {
                            z++;
                        }
                    }

                    JSONArray castArray = creditsObject.getJSONArray(TmdbApiConstants.MOVIE_CAST);
                    int k = (castArray.length() > 5) ? 5 : castArray.length();
                    for (int j = 0; j < k; j++) {
                        JSONObject actorObject = castArray.optJSONObject(j);
                        String actorName = actorObject.optString(TmdbApiConstants.MOVIE_ACTOR_NAME);
                        cast = cast + actorName + ExtractDataConstants.WORDS_SEPARATOR + ExtractDataConstants.SPACE_CHARACTER;
                    }
                    // Remove the words separator from the cast string.
                    int castIndex = cast.lastIndexOf(ExtractDataConstants.WORDS_SEPARATOR);
                    if (castIndex != -1) {
                        cast = cast.substring(0, castIndex) + ExtractDataConstants.DOT_CHARACTER;
                    }
                }
                // 10. Extract the movie's reviews.
                ArrayList<Review> reviews = extractReviews(movieReviewsRequestUrl);

                // 11. Extract the movie's similar movies.
                ArrayList<Movie> similarMovies = extractSimilarMovies(movieSimilarMoviesRequestUrl);

                // Create a new Movie instance from the extracted data.
                movie = new Movie.Builder()
                        .setId(id)
                        .setPosterPath(posterPath)
                        .setTitle(title)
                        .setGenres(genres)
                        .setReleaseDate(releaseDate)
                        .setRatingNumber(rating)
                        .setDescription(description)
                        .setDirector(director)
                        .setCast(cast)
                        .setReviewList(reviews)
                        .setSimilarMoviesList(similarMovies)
                        .build();
            }
        } catch (JSONException | NullPointerException e) {
            Log.e(ExtractDataConstants.ERROR_LOG_TAG, ExtractDataConstants.ERROR_LOG_MESSAGE, e);
        }

        // Finally return the movie.
        return movie;
    }

    /**
     * Return a list of {@link Review} objects that has been built up from parsing the given
     * JSON response.
     */
    private static ArrayList<Review> extractReviews(String requestUrl) {

        // Try to perform an HTTP request to the TMDB endpoint URL and receive a JSON response back.
        // In case of an error, print a message to the logs.
        String jsonResponse = null;
        try {
            jsonResponse = MoviesHttpRequest.perform(requestUrl);
        } catch (IOException e) {
            Log.e(ExtractDataConstants.ERROR_LOG_TAG, ExtractDataConstants.ERROR_LOG_MESSAGE, e);
        }

        // Create an empty List that we can start adding reviews to.
        ArrayList<Review> reviews = new ArrayList<>();

        // Try to parse the JSON response string. If there's a problem with the way the JSON
        // is formatted, catch the JSONException exception object that will be thrown so the
        // app doesn't crash, and print the error message to the logs.
        try {
            if (jsonResponse != null) {
                // Convert the JSON response String into a JSONObject.
                JSONObject jsonRootObject = new JSONObject(jsonResponse);

                // Get the instance of the "results" JSONArray which contains the movie objects.
                JSONArray resultsArray = jsonRootObject.optJSONArray(TmdbApiConstants.MOVIES_RESULTS);

                if (resultsArray != null) {
                    // Loop through each movie's review object in the "results" array.
                    for (int i = 0; i < resultsArray.length(); i++) {
                        // Get the movie JSONObject at position i.
                        JSONObject movieObject = resultsArray.getJSONObject(i);

                        // 1. Extract "author" for review's author.
                        // 2. Extract "content" for review's content.
                        String author = movieObject.optString(TmdbApiConstants.REVIEW_AUTHOR);
                        String content = movieObject.optString(TmdbApiConstants.REVIEW_CONTENT);

                        // Create a new Review instance from the extracted data.
                        Review review = new Review.Builder()
                                .setReviewAuthor(author)
                                .setReviewContent(content)
                                .build();

                        // Add the review to the list of reviews.
                        reviews.add(review);
                    }
                }
            }
        } catch (JSONException | NullPointerException e) {
            Log.e(ExtractDataConstants.ERROR_LOG_TAG, ExtractDataConstants.ERROR_LOG_MESSAGE, e);
        }

        // Finally return the list of reviews.
        return reviews;
    }

    /**
     * Return the similar movies list of a given {@link Movie} object that has been built up from
     * parsing the given JSON response.
     */
    private static ArrayList<Movie> extractSimilarMovies(String requestUrl) {

        // Try to perform an HTTP request to the TMDB endpoint URL and receive a JSON response back.
        // In case of an error, print a message to the logs.
        String jsonResponse = null;
        try {
            jsonResponse = MoviesHttpRequest.perform(requestUrl);
        } catch (IOException e) {
            Log.e(ExtractDataConstants.ERROR_LOG_TAG, ExtractDataConstants.ERROR_LOG_MESSAGE, e);
        }

        // Create an empty List that we can start adding reviews to.
        ArrayList<Movie> similarMoviesList = new ArrayList<>();

        // Try to parse the JSON response string. If there's a problem with the way the JSON
        // is formatted, catch the JSONException exception object that will be thrown so the
        // app doesn't crash, and print the error message to the logs.
        try {
            if (jsonResponse != null) {
                // Convert the JSON response String into a JSONObject.
                JSONObject jsonRootObject = new JSONObject(jsonResponse);

                // Get the instance of the "results" JSONArray which contains the movie objects.
                JSONArray resultsArray = jsonRootObject.optJSONArray(TmdbApiConstants.MOVIES_RESULTS);

                if (resultsArray != null) {
                    // Loop through each movie object in the "results" array.
                    for (int i = 0; i < resultsArray.length(); i++) {
                        // Get the movie JSONObject at position i.
                        JSONObject movieObject = resultsArray.getJSONObject(i);

                        // 1. Extract "id" for movie's id.
                        // 2. Extract "poster_path" for movie's title.
                        // 3. Extract "title" for movie's title.
                        // 4. Extract "release_date" for movie's release date.
                        // 5. Extract "vote_average" for movie's rating number.
                        int id = movieObject.optInt(TmdbApiConstants.MOVIE_ID);
                        String posterPath = movieObject.optString(TmdbApiConstants.MOVIE_POSTER_PATH);
                        String title = movieObject.optString(TmdbApiConstants.MOVIE_TITLE);
                        String releaseDate = movieObject.optString(TmdbApiConstants.MOVIE_RELEASE_DATE);
                        Long rating = movieObject.optLong(TmdbApiConstants.MOVIE_VOTE_AVERAGE);

                        // Create a new Movie instance from the extracted data.
                        Movie movie = new Movie.Builder()
                                .setId(id)
                                .setPosterPath(posterPath)
                                .setTitle(title)
                                .setReleaseDate(releaseDate)
                                .setRatingNumber(rating)
                                .build();

                        // Add the movie to the list of movies.
                        similarMoviesList.add(movie);
                    }
                }
            }
        } catch (JSONException e) {
            Log.e(ExtractDataConstants.ERROR_LOG_TAG, ExtractDataConstants.ERROR_LOG_MESSAGE, e);
        }

        // Finally return the list of reviews.
        return similarMoviesList;
    }
}
