package com.nikolaosfragkiadakis.www.movierama.uiadapters;

/* MIT License

Copyright 2020 © Nikolaos Fragkiadakis / www.nikolaosfragkiadakis.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nikolaosfragkiadakis.www.movierama.R;
import com.nikolaosfragkiadakis.www.movierama.datamodel.dataset.Review;

import java.util.ArrayList;

/**
 * This is the custom Reviews Adapter.
 */
public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.MyViewHolder> {
    private Context mContext;
    private ArrayList<Review> mData;

    public ReviewsAdapter(Context mContext, ArrayList<Review> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater mLayoutInflater = LayoutInflater.from(mContext);
        view = mLayoutInflater.inflate(R.layout.review_list_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        // Update the UI with the review's data set.
        holder.authorTextView.setText(mData.get(position).getReviewAuthor());
        holder.contentTextView.setText(mData.get(position).getReviewContent());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public ArrayList<Review> getReviews() {
        return mData;
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView authorTextView;
        TextView contentTextView;

        MyViewHolder(View itemView) {
            super(itemView);

            authorTextView = itemView.findViewById(R.id.review_author);
            contentTextView = itemView.findViewById(R.id.review_content);
        }
    }
}
