package com.nikolaosfragkiadakis.www.movierama.datamodel.datapersistence;

/* MIT License

Copyright 2020 © Nikolaos Fragkiadakis / www.nikolaosfragkiadakis.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.nikolaosfragkiadakis.www.movierama.utilities.Constants.MovieEntryConstants;

/**
 * This is the contract of the "movies" database, which will be used in order to
 * save the user's "favorites" movies locally.
 */
public final class MovieContract {

    public static abstract class MovieEntry implements BaseColumns {
        // Figure out if a movie is a favorite item.
        public static boolean isFavorite(Context context, int movieID) {
            // Get a readable database instance.
            MovieDbHelper movieDbHelper = new MovieDbHelper(context);
            SQLiteDatabase db = movieDbHelper.getReadableDatabase();

            // Create the query that will be executed.
            String query = "SELECT * FROM favorites WHERE " + MovieEntryConstants.COLUMN_MOVIE_ID + "=" + movieID + ";";

            // Execute the query.
            Cursor cursor = db.rawQuery(query, null);

            // Return false if the movie is not a favorite item or true if it is.
            if (cursor.getCount() <= 0) {
                cursor.close();
                return false;
            } else {
                cursor.close();
                return true;
            }
        }

        // Add a movie to the favorite list.
        public static void addFavorite(Context context, int movieID) {
            // Get a writable database instance.
            MovieDbHelper movieDbHelper = new MovieDbHelper(context);
            SQLiteDatabase db = movieDbHelper.getWritableDatabase();

            // Create the query that will be executed.
            String query = "INSERT INTO favorites(" + MovieEntryConstants.COLUMN_MOVIE_ID + ") VALUES(" + movieID + ");";

            // Execute the query.
            db.execSQL(query);
        }

        // Remove a movie from the favorite list.
        public static void removeFavorite(Context context, int movieID) {
            // Get a writable database instance.
            MovieDbHelper movieDbHelper = new MovieDbHelper(context);
            SQLiteDatabase db = movieDbHelper.getWritableDatabase();

            // Create the query that will be executed.
            String query = "DELETE FROM favorites WHERE " + MovieEntryConstants.COLUMN_MOVIE_ID + "=" + movieID + ";";

            // Execute the query.
            db.execSQL(query);
        }
    }
}
