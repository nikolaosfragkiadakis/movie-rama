package com.nikolaosfragkiadakis.www.movierama.uiadapters;

/* MIT License

Copyright 2020 © Nikolaos Fragkiadakis / www.nikolaosfragkiadakis.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nikolaosfragkiadakis.www.movierama.R;
import com.nikolaosfragkiadakis.www.movierama.datamodel.dataset.Movie;
import com.nikolaosfragkiadakis.www.movierama.utilities.Constants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * This is the custom Similar Movies Adapter.
 */
public class SimilarMoviesAdapter extends RecyclerView.Adapter<SimilarMoviesAdapter.MyViewHolder> {
    private Context mContext;
    private ArrayList<Movie> mSimilarMoviesList;
    private OnSimilarMovieListener mOnSimilarMovieListener;

    public SimilarMoviesAdapter(Context mContext, ArrayList<Movie> similarMoviesList, OnSimilarMovieListener onSimilarMovieListener) {
        this.mContext = mContext;
        this.mSimilarMoviesList = similarMoviesList;
        this.mOnSimilarMovieListener = onSimilarMovieListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater mLayoutInflater = LayoutInflater.from(mContext);
        view = mLayoutInflater.inflate(R.layout.similar_movie_list_item, parent, false);
        return new MyViewHolder(view, mOnSimilarMovieListener);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        // Update the UI with the movie's data set.
        holder.titleTextView.setText(mSimilarMoviesList.get(position).getTitle());
        holder.releaseDateTextView.setText(mSimilarMoviesList.get(position).getReleaseDate());
        holder.ratingBar.setRating(mSimilarMoviesList.get(position).getRatingNumber());

        // Download the movie's poster using the "Picasso" library
        // in a attempt to strive for better performance.
        Picasso.get().load(Constants.SimilarMoviesAdapterConstants.PREFIX_OF_POSTER_REQUEST_URL +
                mSimilarMoviesList.get(position).getPosterPath()).into(holder.posterImageView);
    }

    @Override
    public int getItemCount() {
        return mSimilarMoviesList.size();
    }

    public Movie getMovie(int position) {
        return mSimilarMoviesList.get(position);
    }

    public ArrayList<Movie> getMovies() {
        return mSimilarMoviesList;
    }

    /**
     * The purpose of the implementation of "OnSimilarMovieListener" interface is to help
     * in attaching an onClickListener in every ViewHolder item and identify on which
     * one the click has been occurred.
     */
    public interface OnSimilarMovieListener {
        void onSimilarMovieClick(int position);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView posterImageView;
        TextView titleTextView;
        TextView releaseDateTextView;
        RatingBar ratingBar;

        OnSimilarMovieListener onSimilarMovieListener;

        MyViewHolder(View itemView, OnSimilarMovieListener onSimilarMovieListener) {
            super(itemView);

            posterImageView = itemView.findViewById(R.id.similar_movie_poster);
            titleTextView = itemView.findViewById(R.id.similar_movie_title);
            releaseDateTextView = itemView.findViewById(R.id.similar_movie_release_date);
            ratingBar = itemView.findViewById(R.id.similar_movie_rating);

            this.onSimilarMovieListener = onSimilarMovieListener;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onSimilarMovieListener.onSimilarMovieClick(getAdapterPosition());
        }
    }
}
