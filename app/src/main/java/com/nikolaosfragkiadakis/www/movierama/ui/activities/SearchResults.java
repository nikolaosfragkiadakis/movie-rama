package com.nikolaosfragkiadakis.www.movierama.ui.activities;

/* MIT License

Copyright 2020 © Nikolaos Fragkiadakis / www.nikolaosfragkiadakis.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.nikolaosfragkiadakis.www.movierama.uiadapters.MoviesAdapter;
import com.nikolaosfragkiadakis.www.movierama.dataloaders.MoviesLoader;
import com.nikolaosfragkiadakis.www.movierama.datamodel.dataset.Movie;
import com.nikolaosfragkiadakis.www.movierama.R;
import com.nikolaosfragkiadakis.www.movierama.utilities.Constants.SearchResultsConstants;
import com.nikolaosfragkiadakis.www.movierama.infinitescroll.InfiniteScrollListener;
import com.nikolaosfragkiadakis.www.movierama.utilities.NetworkCheck;

import java.util.ArrayList;

/**
 * The {@link SearchResults} is the search results screen of the Movie Rama app,
 * which provides the UI with the movies' data of the user's query from the TMDB JSON API.
 */
public class SearchResults extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ArrayList<Movie>>, MoviesAdapter.OnMovieListener {
    // The movies' final composed search query URL from the TMDB JSON API.
    private static String finalSearchMoviesRequestURL;

    // The reference to the LoaderManager.
    private LoaderManager mLoaderManager;

    // The user's search query.
    private String userQuery;

    // The adapter for the movies' list.
    private MoviesAdapter mAdapter;

    // The recycler view for the movies' list.
    private RecyclerView mRecyclerView;

    // The reference to the InfiniteScrollListener.
    private InfiniteScrollListener mRecyclerViewInfiniteScrollListener;

    // The reference to the SwipeRefreshLayout.
    private SwipeRefreshLayout mSwipeRefreshLayout;

    // The reference to the Intent instance.
    private Intent intent;

    // The reference to the Toast instance.
    private Toast toast;

    // The reference to the ProgressBar.
    private ProgressBar progressBar;

    // The reference to the Empty state TextView.
    private TextView emptyStateTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_screen);

        // Set the visible title of the Search Results activity programmatically.
        this.setTitle(R.string.search_results_activity_name);

        // Create the fully composed search query request URL by concatenate its components.
        intent = getIntent();
        String query = intent.getStringExtra(SearchResultsConstants.EXTRA_USER_NEW_QUERY_FIELD_TAG);
        if (query != null) {
            userQuery = query.replace(SearchResultsConstants.SPACE_CHARACTER, SearchResultsConstants.PLUS_CHARACTER);
            finalSearchMoviesRequestURL = SearchResultsConstants.PREFIX_OF_MOVIE_SEARCH_REQUEST_URL +
                    userQuery + SearchResultsConstants.SUPPLEMENTARY_QUERY_OF_MOVIE_SEARCH_REQUEST_URL;
        }

        if (NetworkCheck.isNetworkConnected(SearchResults.this)) {
            // There is an internet connection, thus the movies' data can get downloaded.

            // Find a reference to the RecyclerView in the layout.
            mRecyclerView = findViewById(R.id.recycler_view);

            // Create a new ArrayAdapter of movies.
            mAdapter = new MoviesAdapter(this, new ArrayList<>(), this);

            // Set the layout manager on the RecyclerView and specify how the data set
            // will be displayed in the UI.
            LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
            mRecyclerView.setLayoutManager(mLinearLayoutManager);

            // Set the adapter on the RecyclerView so the list can be populated in the UI.
            mRecyclerView.setAdapter(mAdapter);

            // Get a reference to the LoaderManager, in order to interact with loaders.
            mLoaderManager = getLoaderManager();

            // Initialize the loader. Pass in the int ID constant defined above and pass in null for
            // the bundle. Pass in this activity for the LoaderCallbacks parameter ( which is valid
            // because this activity implements the LoaderCallbacks interface ).
            mLoaderManager.initLoader(SearchResultsConstants.SEARCH_MOVIES_LOADER_ID, null, this).forceLoad();

            // Set the InfiniteScrollListener to the RecyclerView.
            mRecyclerViewInfiniteScrollListener = new InfiniteScrollListener(mLinearLayoutManager) {
                @Override
                public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                    loadMoreMovies(page);
                }
            };

            mRecyclerView.addOnScrollListener(mRecyclerViewInfiniteScrollListener);
        } else {
            // There's no internet connection. Hide the loading indicator because
            // the data cannot get loaded.
            progressBar = findViewById(R.id.progress_bar);
            progressBar.setVisibility(ProgressBar.GONE);

            // Show an appropriate message in the UI.
            emptyStateTextView = findViewById(R.id.empty_view);
            emptyStateTextView.setText(R.string.no_internet_connection);
        }

        // Find a reference to the SearchView in the layout.
        SearchView mSearchView = findViewById(R.id.search_view);

        // Set an "OnQueryTextListener" in order a potential query from the user could get caught.
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                submitUserQuery(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        // Find a reference to the SwipeRefreshLayout in the layout.
        mSwipeRefreshLayout = findViewById(R.id.swipe_to_refresh);

        // Set "refresh indicator's" color.
        mSwipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent),
                getResources().getColor(R.color.colorPrimary));

        // Set an "OnRefreshListener" in order to give at the user the functionality
        // to perform a swipe-to-refresh gesture.
        // Refresh the UI.
        mSwipeRefreshLayout.setOnRefreshListener(this::refresh);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu.
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // User clicked on the back button.
        if (!mAdapter.getMovies().isEmpty()) {
            // The movies' list is not empty, thus the movies' data can be cleaned up.

            // Respond to a click on the "Up" arrow button in the app bar.
            DialogInterface.OnClickListener discardButtonClickListener =
                    (dialogInterface, i) -> {
                        // User clicked the "Return" button, navigate to parent activity.
                        // Show an appropriate message in the UI.
                        toast = Toast.makeText(SearchResults.this, R.string.confirmation_toast_message, Toast.LENGTH_LONG);
                        toast.show();
                        finish();
                    };
            // Show a dialog that notifies the user that the search results will get lost.
            showSearchResultsWillGetLostDialog(discardButtonClickListener);
        } else {
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Save the current state of the SearchResults.
        SharedPreferences prefs = getSharedPreferences(SearchResultsConstants.PREF_ACTIVITY_NAME_FIELD_TAG, MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(SearchResultsConstants.PREF_LAST_ACTIVITY_FIELD_TAG, getClass().getName());
        editor.apply();
    }

    /**
     * Set up an Async Task Loader in order to perform the HTTP Request.
     */
    @Override
    public Loader<ArrayList<Movie>> onCreateLoader(int i, Bundle bundle) {
        // Create a new loader for the given URL.
        if (i == 1) {
            return new MoviesLoader(SearchResults.this, finalSearchMoviesRequestURL +
                    SearchResultsConstants.FIRST_PAGE);
        } else {
            return new MoviesLoader(SearchResults.this, finalSearchMoviesRequestURL);
        }
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<Movie>> loader, ArrayList<Movie> movies) {
        // Hide loading indicator because the data has been loaded.
        progressBar = findViewById(R.id.progress_bar);
        progressBar.setVisibility(ProgressBar.GONE);

        // If there is no parsed data, the Empty View state text must be showed.
        mRecyclerView.setVisibility(View.GONE);
        emptyStateTextView = findViewById(R.id.empty_view);
        emptyStateTextView.setText(R.string.no_data_found);

        // If the data have successfully been parsed, update the UI.
        if (movies != null && !movies.isEmpty() && mAdapter.getItemCount() == 0) {
            // This is the first load.
            mRecyclerView.setVisibility(View.VISIBLE);
            emptyStateTextView.setVisibility(View.GONE);
            mAdapter = new MoviesAdapter(this, movies, this);
            mRecyclerView.setAdapter(mAdapter);
        } else if (movies != null && !movies.isEmpty()) {
            // These are the follow loads.
            mRecyclerView.setVisibility(View.VISIBLE);
            emptyStateTextView.setVisibility(View.GONE);
            mAdapter.addMovies(movies);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<Movie>> loader) {
        // Clear the existing data set and notify the adapter for the change.
        if (NetworkCheck.isNetworkConnected(SearchResults.this)) {
            mAdapter.getMovies().clear();
            mAdapter.notifyDataSetChanged();

            // Rest the "InfiniteScrollListener" for performing a new search.
            mRecyclerViewInfiniteScrollListener.resetState();
        }
    }

    /**
     * Triggered from the InfiniteScrollListener in order to load more movies' data.
     *
     * @param newPageNumber represents the number of the new page of movies data from the TMDB JSON API to get loaded.
     */
    public void loadMoreMovies(int newPageNumber) {
        if (NetworkCheck.isNetworkConnected(SearchResults.this)) {
            // There is an internet connection, thus the movies' data can get loaded.

            // Show loading indicator till the data has been loaded.
            progressBar = findViewById(R.id.progress_bar);
            progressBar.setVisibility(ProgressBar.VISIBLE);

            // Create the fully composed load more movies' request URL by concatenate its components.
            finalSearchMoviesRequestURL = SearchResultsConstants.PREFIX_OF_MOVIE_SEARCH_REQUEST_URL +
                    userQuery + SearchResultsConstants.SUPPLEMENTARY_QUERY_OF_MOVIE_SEARCH_REQUEST_URL + newPageNumber;

            // Get a reference to the LoaderManager, in order to interact with loaders.
            mLoaderManager = getLoaderManager();

            // Restart the Movies Loader.
            mLoaderManager.restartLoader(SearchResultsConstants.MORE_SEARCH_MOVIES_LOADER_ID, null, this).forceLoad();
        } else {
            // There is no internet connection, thus the movies' data cannot get loaded.
            // Show an appropriate message in the UI.
            toast = Toast.makeText(this, R.string.error_during_loading_more_movies, Toast.LENGTH_LONG);
            toast.show();
        }
    }

    /**
     * The user has perform a swipe-to-refresh gesture so the UI should get updated.
     */
    public void refresh() {
        // Clear the existing data set and notify the adapter for the change.
        mAdapter.getMovies().clear();
        mAdapter.notifyDataSetChanged();

        // Rest the "InfiniteScrollListener" for performing a new search.
        mRecyclerViewInfiniteScrollListener.resetState();

        // Get a reference to the LoaderManager, in order to interact with loaders.
        mLoaderManager = getLoaderManager();

        // Destroy the LoaderManager in order to be able to get initialized again.
        mLoaderManager.destroyLoader(SearchResultsConstants.SEARCH_MOVIES_LOADER_ID);
        mLoaderManager.destroyLoader(SearchResultsConstants.MORE_SEARCH_MOVIES_LOADER_ID);

        // Show appropriate messages in the UI.
        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setVisibility(View.GONE);
        emptyStateTextView = findViewById(R.id.empty_view);
        emptyStateTextView.setVisibility(View.VISIBLE);
        emptyStateTextView.setText(R.string.search_results_refresh_message);

        // Hide the "refresh" indicator.
        mSwipeRefreshLayout.setRefreshing(false);
    }

    /**
     * In the case that a movies' expert wants to search for a movie by title again, then the new query must get submitted.
     *
     * @param query represents the user's query.
     */
    public void submitUserQuery(String query) {
        if (NetworkCheck.isNetworkConnected(SearchResults.this)) {
            // There is an internet connection, thus the user's search query can get submitted.

            // Hide the empty view and show loading indicator till the data has been loaded.
            emptyStateTextView = findViewById(R.id.empty_view);
            emptyStateTextView.setVisibility(View.GONE);
            progressBar = findViewById(R.id.progress_bar);
            progressBar.setVisibility(ProgressBar.VISIBLE);

            // Clear the existing data set and notify the adapter for the change.
            mAdapter.getMovies().clear();
            mAdapter.notifyDataSetChanged();

            // Reset the "InfiniteScrollListener" for performing a new search.
            mRecyclerViewInfiniteScrollListener.resetState();

            // Create the fully composed search query request URL by concatenate its components.
            if (query != null) {
                userQuery = query.replace(SearchResultsConstants.SPACE_CHARACTER, SearchResultsConstants.PLUS_CHARACTER);
                finalSearchMoviesRequestURL = SearchResultsConstants.PREFIX_OF_MOVIE_SEARCH_REQUEST_URL +
                        userQuery + SearchResultsConstants.SUPPLEMENTARY_QUERY_OF_MOVIE_SEARCH_REQUEST_URL;
            }

            // Get a reference to the LoaderManager, in order to interact with loaders.
            mLoaderManager = getLoaderManager();

            // Destroy the LoaderManager in order to able to get initialized again.
            mLoaderManager.destroyLoader(SearchResultsConstants.SEARCH_MOVIES_LOADER_ID);

            // Initialize the Movies Loader again.
            mLoaderManager.initLoader(SearchResultsConstants.SEARCH_MOVIES_LOADER_ID, null, this).forceLoad();
        } else {
            //There's no internet connection, thus the user's search query cannot get submitted..
            // Show an appropriate message in the UI.
            toast = Toast.makeText(this, R.string.error_during_submitting_search_query, Toast.LENGTH_LONG);
            toast.show();
        }
    }

    /**
     * Set a dialog message in order to notify the user that the search results will get lost,
     * in the case of the returning at the home screen.
     *
     * @param discardButtonClickListener the onClick listener for the "Keep searching" button.
     */
    @SuppressLint("InflateParams")
    private void showSearchResultsWillGetLostDialog(DialogInterface.OnClickListener discardButtonClickListener) {
        // Create an AlertDialog.Builder and set the message, and click listeners
        // for the positive and negative buttons on the dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.dialog_view, null));
        builder.setPositiveButton(R.string.confirm_button, discardButtonClickListener);
        builder.setNegativeButton(R.string.discard_button, (dialogInterface, i) -> {
            // User clicked the "Keep searching" button, so dismiss
            // the dialog and continue let the user to continue searching for movies.
            if (dialogInterface != null) {
                dialogInterface.dismiss();
            }
        });

        // Create and show the AlertDialog.
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    /**
     * In the case that a curious user wants to know more about a movie, then its details must be shown.
     *
     * @param position represents the position of the clicked movie.
     */
    @Override
    public void onMovieClick(int position) {
        if (NetworkCheck.isNetworkConnected(SearchResults.this)) {
            // There is an internet connection, thus the movie's data can get loaded.
            intent = new Intent(this, MovieDetails.class);
            intent.putExtra(SearchResultsConstants.EXTRA_MOVIE_ID_FIELD_TAG, mAdapter.getMovie(position).getID());
            startActivity(intent);
        } else {
            // There is no internet connection, thus the movie's data cannot get loaded.
            // Show an appropriate message in the UI.
            toast = Toast.makeText(this, R.string.error_during_loading_single_movie_data, Toast.LENGTH_LONG);
            toast.show();
        }
    }
}
