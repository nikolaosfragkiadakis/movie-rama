package com.nikolaosfragkiadakis.www.movierama.uiadapters;

/* MIT License

Copyright 2020 © Nikolaos Fragkiadakis / www.nikolaosfragkiadakis.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.nikolaosfragkiadakis.www.movierama.datamodel.dataset.Movie;
import com.nikolaosfragkiadakis.www.movierama.R;
import com.nikolaosfragkiadakis.www.movierama.utilities.Constants.MoviesAdapterConstants;
import com.nikolaosfragkiadakis.www.movierama.datamodel.datapersistence.MovieContract.MovieEntry;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * This is the custom Movies Adapter.
 */
public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MyViewHolder> {
    private Context mContext;
    private ArrayList<Movie> mData;
    private OnMovieListener mOnMovieListener;

    public MoviesAdapter(Context mContext, ArrayList<Movie> mData, OnMovieListener onMovieListener) {
        this.mContext = mContext;
        this.mData = mData;
        this.mOnMovieListener = onMovieListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater mLayoutInflater = LayoutInflater.from(mContext);
        view = mLayoutInflater.inflate(R.layout.movie_list_item, parent, false);
        return new MyViewHolder(view, mOnMovieListener);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        // Update the UI with the movie's data set.
        holder.titleTextView.setText(mData.get(position).getTitle());
        holder.releaseDateTextView.setText(mData.get(position).getReleaseDate());
        holder.ratingBar.setRating(mData.get(position).getRatingNumber());

        // Download the movie's poster using the "Picasso" library
        // in a attempt to strive for better performance.
        Picasso.get().load(MoviesAdapterConstants.PREFIX_OF_POSTER_REQUEST_URL +
                mData.get(position).getPosterPath()).into(holder.posterImageView);

        // Try to figure out if the current movie has been added to the favorites
        // table by the user.
        if (!MovieEntry.isFavorite(mContext, mData.get(position).getID())) {
            holder.favoriteImageView.setImageResource(R.drawable.ic_favorite_outline);
        } else {
            holder.favoriteImageView.setImageResource(R.drawable.ic_favorite_filled);
        }

        // Attach an "OnClickListener" in the favorite button.
        holder.favoriteImageView.setOnClickListener(view -> {
            if (!MovieEntry.isFavorite(mContext, mData.get(position).getID())) {
                MovieEntry.addFavorite(mContext, mData.get(position).getID());
                holder.favoriteImageView.setImageResource(R.drawable.ic_favorite_filled);
                Toast.makeText(mContext, R.string.favorite_added, Toast.LENGTH_SHORT).show();
            } else {
                MovieEntry.removeFavorite(mContext, mData.get(position).getID());
                holder.favoriteImageView.setImageResource(R.drawable.ic_favorite_outline);
                Toast.makeText(mContext, R.string.favorite_removed, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public Movie getMovie(int position) {
        return mData.get(position);
    }

    public ArrayList<Movie> getMovies() {
        return mData;
    }

    public void addMovies(ArrayList<Movie> newMovies) {
        mData.addAll(mData.size(), newMovies);
    }

    /**
     * The purpose of the implementation of "OnSimilarMovieListener" interface is to help
     * in attaching an onClickListener in every ViewHolder item and identify on which
     * one the click has been occurred.
     */
    public interface OnMovieListener {
        void onMovieClick(int position);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView posterImageView;
        TextView titleTextView;
        TextView releaseDateTextView;
        RatingBar ratingBar;
        ImageView favoriteImageView;
        CardView cardView;

        OnMovieListener onMovieListener;

        MyViewHolder(View itemView, OnMovieListener onMovieListener) {
            super(itemView);

            posterImageView = itemView.findViewById(R.id.poster);
            titleTextView = itemView.findViewById(R.id.title);
            releaseDateTextView = itemView.findViewById(R.id.release_date);
            ratingBar = itemView.findViewById(R.id.rating);
            favoriteImageView = itemView.findViewById(R.id.favorite);
            cardView = itemView.findViewById(R.id.movie_list_item_card_view);

            this.onMovieListener = onMovieListener;

            favoriteImageView.setOnClickListener(this);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onMovieListener.onMovieClick(getAdapterPosition());
        }
    }
}
