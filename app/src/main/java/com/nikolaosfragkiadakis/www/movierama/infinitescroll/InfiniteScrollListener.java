package com.nikolaosfragkiadakis.www.movierama.infinitescroll;

/* MIT License

Copyright 2020 © Nikolaos Fragkiadakis / www.nikolaosfragkiadakis.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

        import androidx.recyclerview.widget.GridLayoutManager;
        import androidx.recyclerview.widget.LinearLayoutManager;
        import androidx.recyclerview.widget.RecyclerView;
        import androidx.recyclerview.widget.StaggeredGridLayoutManager;

        import org.jetbrains.annotations.NotNull;

/**
 * The {@link InfiniteScrollListener} abstract class represents a custom OnScrollListener
 * for the home screen's RecyclerView.
 *
 * After reading a lot and trying a few different solutions that did not actually ever
 * worked properly, I end up that this one fits best with MovieRama app. Thus, I implemented
 * the code of this abstract class from the "Endless Scrolling with AdapterViews and RecyclerView"
 * codepath.com wiki on GitHub.
 *
 * https://github.com/codepath/android_guides/wiki/Endless-Scrolling-with-AdapterViews-and-RecyclerView
 * https://gist.github.com/nesquena/d09dc68ff07e845cc622
 */
public abstract class InfiniteScrollListener extends RecyclerView.OnScrollListener {

    // The current offset index of the movie's data that have been loaded.
    private int currentPage = 1;

    // The total number of the movie's data in the data set after the last load.
    private int previousTotalItemCount = 0;

    // True if the last set of data is still getting loaded.
    private boolean loading = true;

    // Sets the starting page index.
    private int startingPageIndex = 1;

    // The reference to the LayoutManager.
    private RecyclerView.LayoutManager mLayoutManager;

    protected InfiniteScrollListener(LinearLayoutManager layoutManager) {
        this.mLayoutManager = layoutManager;
    }

    private int getLastVisibleItem(int[] lastVisibleItemPositions) {
        int maxSize = 0;
        for (int i = 0; i < lastVisibleItemPositions.length; i++) {
            if (i == 0) {
                maxSize = lastVisibleItemPositions[i];
            } else if (lastVisibleItemPositions[i] > maxSize) {
                maxSize = lastVisibleItemPositions[i];
            }
        }
        return maxSize;
    }

    /**
     * If there are not previous data getting loaded and the user has reached while scrolling
     * the end of the movie's list, load more movie's data.
     */
    @Override
    public void onScrolled(@NotNull RecyclerView view, int dx, int dy) {
        int lastVisibleItemPosition = 0;
        int totalItemCount = mLayoutManager.getItemCount();

        if (mLayoutManager instanceof StaggeredGridLayoutManager) {
            int[] lastVisibleItemPositions = ((StaggeredGridLayoutManager) mLayoutManager).findLastVisibleItemPositions(null);
            // Get maximum element within the list.
            lastVisibleItemPosition = getLastVisibleItem(lastVisibleItemPositions);
        } else if (mLayoutManager instanceof GridLayoutManager) {
            lastVisibleItemPosition = ((GridLayoutManager) mLayoutManager).findLastVisibleItemPosition();
        } else if (mLayoutManager instanceof LinearLayoutManager) {
            lastVisibleItemPosition = ((LinearLayoutManager) mLayoutManager).findLastVisibleItemPosition();
        }

        // If the total item count is zero and the previous isn't, then must be assumed that
        // the movie's list is invalidated and thus it should be set back to its initial state.
        if (totalItemCount < previousTotalItemCount) {
            this.currentPage = this.startingPageIndex;
            this.previousTotalItemCount = 0;
            if (totalItemCount == 0) {
                this.loading = true;
            }
        }

        // If the movies' data are getting still loaded, check if the data set count has changed,
        // and update the current page number and total item count.
        if (loading && (totalItemCount > previousTotalItemCount)) {
            loading = false;
            previousTotalItemCount = totalItemCount;
        }

        // The minimum amount of movie's list items to be below the current scroll position
        // before get loaded more.
        int visibleThreshold = 5;

        // If it isn’t currently loading, we check to see if we have breached
        // the visibleThreshold and need to reload more data.
        // If we do need to reload some more data, we execute onLoadMore to fetch the data.
        // threshold should reflect how many total columns there are too.
        if (!loading && (lastVisibleItemPosition + visibleThreshold) >= totalItemCount) {
            currentPage++;
            onLoadMore(currentPage, totalItemCount, view);
            loading = true;
        }
    }

    // This method must get called whenever performing new searches.
    public void resetState() {
        this.currentPage = this.startingPageIndex;
        this.previousTotalItemCount = 0;
        this.loading = true;
    }

    /**
     * The onLoadMore abstract method defines the process for actually loading
     * more movies' data based on page number from the TMDB JSON API.
     */
    public abstract void onLoadMore(int page, int totalItemsCount, RecyclerView view);
}
