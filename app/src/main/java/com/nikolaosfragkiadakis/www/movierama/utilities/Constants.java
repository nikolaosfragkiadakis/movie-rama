package com.nikolaosfragkiadakis.www.movierama.utilities;

/* MIT License

Copyright 2020 © Nikolaos Fragkiadakis / www.nikolaosfragkiadakis.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

import com.nikolaosfragkiadakis.www.movierama.datarequest.ExtractMoviesData;
import com.nikolaosfragkiadakis.www.movierama.ui.activities.MovieDetails;
import com.nikolaosfragkiadakis.www.movierama.ui.activities.PopularMovies;
import com.nikolaosfragkiadakis.www.movierama.ui.activities.SearchResults;
import com.nikolaosfragkiadakis.www.movierama.ui.activities.MoreInformation;
import com.nikolaosfragkiadakis.www.movierama.uiadapters.MoviesAdapter;
import com.nikolaosfragkiadakis.www.movierama.uiadapters.SimilarMoviesAdapter;
import com.nikolaosfragkiadakis.www.movierama.datamodel.datapersistence.MovieContract.MovieEntry;
import com.nikolaosfragkiadakis.www.movierama.datamodel.datapersistence.MovieDbHelper;
import com.nikolaosfragkiadakis.www.movierama.ui.dispatcher.ActivityDispatcher;

/**
 * This class is a container of all the constant values that is related to {@link PopularMovies},
 * {@link SearchResults}, {@link MovieDetails}, {@link MoreInformation} and all the helper classes
 * for requesting and receiving the movies' data from the TMDB JSON API.
 */
public class Constants {
    ////////////////////////////////////////////////////
    /////////////////////// APP ////////////////////////
    ////////////////////////////////////////////////////

    // The developer's API KEY from the the TMDB JSON API.
    private static final String TMDB_API_KEY = "30842f7c80f80bb3ad8a2fb98195544d";

    ////////////////////////////////////////////////////
    //////////////////////// UI ////////////////////////
    ////////////////////////////////////////////////////

    /**
     * The constant values of the {@link PopularMovies} class.
     */
    public static class PopularMoviesConstants {
        // The popular movies' request URL from the TMDB JSON API.
        public static final String POPULAR_MOVIES_REQUEST_URL = "https://api.themoviedb.org/3/movie/popular?api_key=" + TMDB_API_KEY + "&language=en-US&page=1";

        // The popular movies' request URL prefix for load more movies' data from the TMDB JSON API.
        public static final String LOAD_MORE_MOVIES_REQUEST_URL = "https://api.themoviedb.org/3/movie/popular?api_key=" + TMDB_API_KEY + "&language=en-US&page=";

        // Constant value for the first movies loader ID.
        public static final int MOVIES_LOADER_ID = 1;

        // Constant value for the next movies loader ID.
        public static final int MORE_MOVIES_LOADER_ID = 2;

        // Intent's extra field tag for movie's ID.
        public static final String EXTRA_MOVIE_ID_FIELD_TAG = "Movie's ID";

        // Intent's extra field tag for user's query ID.
        public static final String EXTRA_USER_QUERY_FIELD_TAG = "User's query";

        // Shared Preferences activity's name tag.
        public static final String PREF_ACTIVITY_NAME_FIELD_TAG = "PopularMovies";

        // Shared Preferences last activity tag.
        public static final String PREF_LAST_ACTIVITY_FIELD_TAG = "lastActivity";
    }

    /**
     * The constant values of the {@link SearchResults} class.
     */
    public static class SearchResultsConstants {
        // The movie's search query prefix of request URL from the TMDB JSON API.
        public static final String PREFIX_OF_MOVIE_SEARCH_REQUEST_URL = "https://api.themoviedb.org/3/search/movie?include_adult=false&language=en-US&api_key=" + TMDB_API_KEY + "&query=";

        // The movie's search query supplementary query of request URL from the TMDB JSON API.
        public static final String SUPPLEMENTARY_QUERY_OF_MOVIE_SEARCH_REQUEST_URL = "&page=";

        // The number of the first page to load.
        public static final int FIRST_PAGE = 1;

        // Constant value for the first movies results loader ID.
        public static final int SEARCH_MOVIES_LOADER_ID = 3;

        // Constant value for the next movies results loader ID.
        public static final int MORE_SEARCH_MOVIES_LOADER_ID = 4;

        // Intent's extra field tag for movie's ID.
        public static final String EXTRA_MOVIE_ID_FIELD_TAG = "Movie's ID";

        // Intent's extra field tag for user's query ID.
        public static final String EXTRA_USER_NEW_QUERY_FIELD_TAG = "User's query";

        // Shared Preferences activity's name tag.
        public static final String PREF_ACTIVITY_NAME_FIELD_TAG = "SearchResults";

        // Shared Preferences last activity tag.
        public static final String PREF_LAST_ACTIVITY_FIELD_TAG = "lastActivity";

        // Space character tag.
        public static final char SPACE_CHARACTER = ' ';

        // Plus character tag.
        public static final char PLUS_CHARACTER = '+';
    }

    /**
     * The constant values of the {@link MovieDetails} class.
     */
    public static class MovieDetailsConstants {
        // The movie's details prefix of request URL from the TMDB JSON API.
        public static final String PREFIX_OF_MOVIE_DETAILS_REQUEST_URL = "https://api.themoviedb.org/3/movie/";

        // The movie's details supplementary query of request URL from the TMDB JSON API.
        public static final String SUPPLEMENTARY_QUERY_OF_MOVIE_DETAILS_REQUEST_URL = "?api_key=" + TMDB_API_KEY + "&append_to_response=credits";

        // The prefix of the movie's poster URL on the TMDB JSON API.
        public static final String PREFIX_OF_POSTER_REQUEST_URL = "https://image.tmdb.org/t/p/w500";

        // The movie's reviews prefix of request URL from the TMDB JSON API.
        public static final String PREFIX_OF_REVIEWS_REQUEST_URL = "https://api.themoviedb.org/3/movie/";

        // The movie's reviews supplementary query of request URL from the TMDB JSON API.
        public static final String SUPPLEMENTARY_QUERY_OF_REVIEWS_REQUEST_URL = "/reviews?api_key=" + TMDB_API_KEY + "&language=en-US&page=1";

        // The movie's similar movies prefix of request URL from the TMDB JSON API.
        public static final String PREFIX_OF_SIMILAR_MOVIES_REQUEST_URL = "https://api.themoviedb.org/3/movie/";

        // The movie's similar movies supplementary query of request URL from the TMDB JSON API.
        public static final String SUPPLEMENTARY_QUERY_OF_SIMILAR_MOVIES_REQUEST_URL = "/similar?api_key=" + TMDB_API_KEY + "&language=en-US&page=1";

        // Constant value for the movie loader ID.
        public static final int MOVIE_DETAILS_LOADER_ID = 5;

        // Intent's extra field tag for movie's ID.
        public static final String EXTRA_MOVIE_ID_FIELD_TAG = "Movie's ID";

        // Shared Preferences tags.
        public static final String PREF_ACTIVITY_NAME_FIELD_TAG = "MovieDetails";

        // Shared Preferences last activity tag.
        public static final String PREF_LAST_ACTIVITY_FIELD_TAG = "lastActivity";
    }

    /**
     * The constant values of the {@link MoreInformation} class.
     */
    public static class MoreInformationConstants {
        // Shared Preferences activity's name tag.
        public static final String PREF_ACTIVITY_NAME_FIELD_TAG = "MoreInformation";

        // Shared Preferences last activity tag.
        public static final String PREF_LAST_ACTIVITY_FIELD_TAG = "lastActivity";
    }

    /**
     * The constant values of the {@link ActivityDispatcher} class.
     */
    public static class DispatcherConstants {
        // Shared Preferences tags.
        public static final String PREF_ACTIVITY_NAME_FIELD_TAG = "Activity";

        // Shared Preferences last activity tag.
        public static final String PREF_LAST_ACTIVITY_FIELD_TAG = "lastActivity";
    }

    ////////////////////////////////////////////////////
    ////////////////// Movie Adapter ///////////////////
    ////////////////////////////////////////////////////

    /**
     * The constant values of the {@link MoviesAdapter} class.
     */
    public static class MoviesAdapterConstants {
        // The prefix of the movie's poster URL on the TMDB JSON API.
        public static final String PREFIX_OF_POSTER_REQUEST_URL = "https://image.tmdb.org/t/p/w500";
    }

    /**
     * The constant values of the {@link SimilarMoviesAdapter} class.
     */
    public static class SimilarMoviesAdapterConstants {
        // The prefix of the movie's poster URL on the TMDB JSON API.
        public static final String PREFIX_OF_POSTER_REQUEST_URL = "https://image.tmdb.org/t/p/w500";
    }

    ////////////////////////////////////////////////////
    /////////////////// Data Model /////////////////////
    ////////////////////////////////////////////////////

    /**
     * The constant values of the {@link MovieEntry} class.
     */
    public static class MovieEntryConstants {
        // The name of the favorites table in the movie database.
        public static final String FAVORITES_TABLE_NAME = "favorites";

        // The names of the columns of the favorites table.
        //public static final String _ID = "_id";
        public static final String COLUMN_MOVIE_ID = "movie_id";
    }

    /**
     * The constant values of the {@link MovieDbHelper} class.
     */
    public static class MovieDbHelperConstants {
        // The constant value for the database's name.
        public static final String DATABASE_NAME = "movies.db";

        // The constant value for the database's version. If the database schema get changed,
        // the database version should get incremented as well.
        public static final int DATABASE_VERSION = 2;
    }

    ////////////////////////////////////////////////////
    /////////////// Extract Movies Data ////////////////
    ////////////////////////////////////////////////////

    /**
     * The constant values of the {@link ExtractMoviesData} class.
     */
    public static class ExtractDataConstants {
        // A tag for the log messages.
        public static final String ERROR_LOG_TAG = ExtractMoviesData.class.getSimpleName();

        // A message for the logs.
        public static final String ERROR_LOG_MESSAGE = "An error has been occurred during parsing the movies data!";

        // Use comma in order to separate the words for the genres and
        // the cast members of the movie.
        public static final char WORDS_SEPARATOR = ',';

        // Space character.
        public static final char SPACE_CHARACTER = ' ';

        // Dot character.
        public static final char DOT_CHARACTER = '.';

        // Empty string.
        public static final String EMPTY_STRING_TAG = "";
    }

    /**
     * The constant values for the TMDB JSON API.
     */
    public static class TmdbApiConstants {
        // The reference to the "results" JSON array of the movie objects.
        public static final String MOVIES_RESULTS = "results";

        // The reference to the movie's "id".
        public static final String MOVIE_ID = "id";

        // The reference to the movie's "poster_path".
        public static final String MOVIE_POSTER_PATH = "poster_path";

        // The reference to the movie's "title".
        public static final String MOVIE_TITLE = "title";

        // The reference to the movie's "release_date".
        public static final String MOVIE_RELEASE_DATE = "release_date";

        // The reference to the movie's "vote_average".
        public static final String MOVIE_VOTE_AVERAGE = "vote_average";

        // The reference to the movie's "genres".
        public static final String MOVIE_GENRES = "genres";

        // The reference to the movie's genre "name".
        public static final String MOVIE_GENRE_NAME = "name";

        // The reference to the movie's "overview".
        public static final String MOVIE_OVERVIEW = "overview";

        // The reference to the "credits" JSON object.
        public static final String MOVIE_CREDITS = "credits";

        // The reference to the "crew" JSON array.
        public static final String MOVIE_CREW = "crew";

        // The reference to the "job" title of the movie's crew members.
        public static final String MOVIE_CREW_JOB = "job";

        // The reference to the movie's "Director".
        public static final String MOVIE_DIRECTOR = "Director";

        // The reference to the movie's director "name".
        public static final String MOVIE_DIRECTOR_NAME = "name";

        // The reference to the movie's "cast" JSON array.
        public static final String MOVIE_CAST = "cast";

        // The reference to the movie's actor's "name".
        public static final String MOVIE_ACTOR_NAME = "name";

        // The reference to the movie's review "author".
        public static final String REVIEW_AUTHOR = "author";

        // The reference to the movie's review "content".
        public static final String REVIEW_CONTENT = "content";
    }
}
