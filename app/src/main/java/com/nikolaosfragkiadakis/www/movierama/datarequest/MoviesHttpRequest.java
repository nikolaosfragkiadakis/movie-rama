package com.nikolaosfragkiadakis.www.movierama.datarequest;

/* MIT License

Copyright 2020 © Nikolaos Fragkiadakis / www.nikolaosfragkiadakis.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

import com.nikolaosfragkiadakis.www.movierama.utilities.Constants.ExtractDataConstants;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * This class is a container of all the helper methods that are related to performing an http request
 * to a given URL.
 */
class MoviesHttpRequest {

    /**
     * Perform an HTTP request to a given URL and return a String as the response.
     *
     * @param url                    is the final URL that will be used in order to access the movies's data at
     *                               the TMDB JSON API.
     * @return                       the JSON response as a string.
     * @throws IOException           represents the exception that will be thrown in case of an IOException error.
     * @throws NullPointerException  represents the exception that will be thrown in case of an
     *                               NullPointerException error.
     */
    static String perform(String url) throws IOException, NullPointerException {
        // Store the JSON response.
        String jsonResponse = ExtractDataConstants.EMPTY_STRING_TAG;

        // Return early, if the provided URL is null.
        if (url == null) {
            return jsonResponse;
        }

        // Try to perform the request.
        OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("application/octet-stream");
        RequestBody body = RequestBody.create(mediaType, "{}");
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();

        Response response = client.newCall(request).execute();

        // If the request was successful parse the response. In case of an error,
        // print a message to the logs.
        if (response.isSuccessful()) {
            jsonResponse = response.body().string();
        }

        // Return the JSON response.
        return jsonResponse;
    }
}
